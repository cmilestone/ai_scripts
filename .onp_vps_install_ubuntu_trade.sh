#!/bin/bash
# Ubuntu start install
#
#
# curl -s https://gitlab.com/cmilestone/ai_scripts/raw/master/.config_urls > $HOME/onp/.config_urls && wget -i $HOME/onp/.config_urls -N -q


#Проверка всех загрузок
source $HOME/.onp_alias
# source $HOME/onp/.onp_soft.sh
# source $HOME/onp/.onp_os.sh
        ___onp_config_setup_line

#Лог времени
echo "Обновляем систему"
onpdf
#Добавляем crontab
echo "Добавляем crontab"
onp_addonp_crontab
        ______onp_config_log_line

#Ставим общий сетап
echo "Ставим общий сетап"
onp_config_cli_color
        ___onp_config_setup_line

#Обновляем
onp_soft_full_remove_and_all_update
        ___onp_config_setup_line

####################################################################################################################
#Ставим локаль Ubuntu
onp_os_ubuntu_locale
        ___onp_config_setup_line
####################################################################################################################

#Диск подкачки
onp_config_disk_swap
        ___onp_config_setup_line

#Ставим время
onp_config_time
        ___onp_config_setup_line

#Общие программы
onp_soft_install_soft_trade
        ___onp_config_setup_line





#Простое обновление
onp_soft_simple_update
        ___onp_config_setup_line

# Лог времени
        ______onp_config_log_line


#Консоль цвета
onpnano

#Закладки
onpbashmarks

#строка софта
onpsoft_ubuntu

#AWS s3
# onp_awscli

onp_add_onp_bash

onptradeconf
#удалить ненужное для контроля
onpnetplanbackup && onpremove php* && onpremove varnish* && onpremove apach* && onpuuffnoreboot
opapt npm
sudo npm install -g -y supervisor nodemon express node-binance-api socket.io compression memjs
sudo npm install -g -y update
sudo npm audit fix --force -y
sudo systemctl enable nginx


#Команды далее
onp_config_end_server_help
        ___onp_config_setup_line

#окончание
onp_config_end_update
        ___onp_config_setup_line