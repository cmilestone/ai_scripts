#!/bin/bash
# onp_alias 11
#
#
source $HOME/.bashrc_colors
source $HOME/onp/.onp_config.sh
source $HOME/onp/.onp_conf.sh
source $HOME/onp/.onp_app.sh
source $HOME/onp/.onp_app_crontab.sh
source $HOME/onp/.onp_app_install.sh
source $HOME/onp/.onp_app_list.sh
source $HOME/onp/.onp_app_test.sh
source $HOME/onp/.onp_run.sh
source $HOME/onp/.onp_soft.sh
source $HOME/onp/.onp_os.sh
source $HOME/onp/.onp_help.sh
source $HOME/onp/.onp_app_list.sh
source $HOME/onp/.onp_todo.sh
source $HOME/onp/.onp_custom.sh
# source $HOME/.ssh/.onp_pass.sh
# source $HOME/.dir_colors
# echo "$onp_config_title"
export onp_alias_title="Файл с alias......[OK]"
export onp_app_title="Файл с app......[OK]"

#######################################################################################
#aliases ONP
alias aiba="nano $HOME/.bashrc"
alias l="hstr"
alias r="ranger"
alias n="nnn -H"
alias fd="fdfind"
alias fdh="fdfind --hidden"
alias r_add="ranger --copy-config=all && echo ~~~~~~~~~~~~~~ nano ~/.config/ranger/rc.conf ~~~~~~~~~~~~~  && echo ~~~~~~~~~~~~~~ set show_hidden true  ~~~~~~~~~~~~~"
#/aliases ONP
#######################################################################################



onpupdate_func() {
    (cd ~ && [ -d $HOME/onp/ ] || sudo mkdir -p $HOME/onp/ && cd $HOME/onp/ && [ -f $HOME/onp/.config_urls ] || sudo rm $HOME/onp/.config_urls && bash -c "curl -s https://gitlab.com/cmilestone/ai_scripts/raw/master/.config_urls > $HOME/onp/.config_urls" && wget -i $HOME/onp/.config_urls -N -q)
    onpupdate_files_func
    source $HOME/onp/.onp_alias.sh
    source $HOME/.onp_alias
    # ONAPP_VER_here Приложение ONP обновлено, версия
    echo "Приложение ONP обновлено, версия $ONAPP_VER"
    command_autoupdate_always_func
    # onpapt_list_roll_apps
}
export -f onpupdate_func


onpdfd() {
    sudo apt-get -y -qq install wget sudo curl -y && (mkdir -p $HOME/onp && cd $HOME/onp && curl -s https://gitlab.com/cmilestone/ai_scripts/raw/master/.config_urls > $HOME/onp/.config_urls && wget -i $HOME/onp/.config_urls -N -q) | cp $HOME/onp/.onp_alias.sh $HOME/.onp_alias && source $HOME/.onp_alias && echo "********** Установка прошла успешно ******" && onpd && onpd
    onpupdate_func
}
export -f onpdfd


onpdu() {
    onpdfd
    onpdf
}
export -f onpdu

onpf() {
    source $HOME/.bashrc
}
export -f onpf

onpd() {
    onpdrepo_func
}
export -f onpd


onpsize() {
    sudo resize
}
export -f onpsize

onpdf() {
    onpupdate_func
    onpapt_list_roll_apps
    onptradeconf

}
export -f onpdf


onponp() {
    onpdrepo_func
    onpupdate_func
    onpapt_list_roll_apps
    onptradeconf
    echo "********** для применения настроек onpdfd ******"
}
export -f onponp

onpdrepo() {
    onpdrepo_func
}
export -f onpdrepo


onpv() {
    echo "Версия $ONAPP_VER"
}

onpcat () {
    cat $HOME/.onp_alias
}
export -f onpcat

alniget () {
    rm $HOME/onp/.alni*
}
export -f alniget

alnidel () {
     rm -rf $HOME/onp/.*
}
export -f alnidel


onph() {
    echo "Профиль обновлен, версия $ONAPP_VER"
    echo "
****************************************************
*                   Список команд                  *
****************************************************
    "
    onp-apps-help-list
}
export -f onph

th() {
    echo "Профиль обновлен, версия $ONAPP_VER"
    echo "onptradeconf
****************************************************
*                   Список команд                  *
****************************************************
    "
    onp_trade_help
}
export -f th

onpname() {
    echo "Сервер $ONP_SERVER_NAME"
    # echo "*************************     Список команд    *************************"
}
export -f onpname

onpip() {
    echo -n "IP адрес изнутри: "
    hostname -I | awk '{print $1}'
}
export -f onpip

onpipf() {
    echo "-----------------------------------------------"
    echo -n "Host: "
    hostname
    echo -n "IP адрес изнутри: "
    hostname -I | awk '{print $1}'
    echo "IP адрес изне: "
    echo "-----------------------------------------------"
    curl ipinfo.io | jq '.[]'
    echo "-----------------------------------------------"
}
export -f onpipf

onpsys(){
    echo "-----------------------------------------------"
    onpname
    onpnamecli
    echo "Система: $ONPSYS_VERSION_FULL"
    uname -a
    lsb_release -a
    echo "Версия ONP: $ONAPP_VER"
    echo "-----------------------------------------------"
    echo -n "Host: "
    hostname
    echo -n "IP адрес изнутри: "
    hostname -I | awk '{print $1}'
    echo "IP адрес изне: "
    echo "-----------------------------------------------"
    curl ipinfo.io | jq '.[]'
    echo "-----------------------------------------------"
}
export -f onpsys

onpnamecli() {
    echo "Сервер $ONP_SERVER_NAME_CLI"
    # echo "*************************     Список команд    *************************"
}
export -f onpnamecli


onpfullname() {
    echo "Сервер адресу $ONP_SERVER_NAME_CLI  с системой $ONP_PROMT_NAME, полное имя $ONP_SERVER_NAME"
    # echo "*************************     Список команд    *************************"
}
export -f onpfullname



____aprofile() {
    source $HOME/.profile
    echo "Профиль обновлен, версия $ONAPP_VER"
}
export -f ____aprofile


qwe() {
    echo "~~~~~~~~~~~~~~~~~~~~~ Выход из ONP OS ~~~~~~~~~~~~~~~~~~~~~"
    date
    echo "До новых встреч"
    exit
}
export -f qwe


aissh() {
    echo "onpssh >>>>> настройка"
    source $HOME/.ssh/.onp_pub.sh
    onp_pub_func_echo
    onp_pub_func_run
}
export -f aissh

onpssh() {
    nano $HOME/.ssh/.onp_pub.sh
}
export -f onpssh

bashrcbackup() {
    echo "~~~~~~~~~~~~~~~~~~~~~ Создание копии .bashrc  ~~~~~~~~~~~~~~~~~~~~~"
    cd ~
    cp /~/.bashrc /~/.bashrc_backup
    echo "Копия .bashrc выполнена"

}
export -f bashrcbackup


onplistrepo() {
    ll /etc/apt/sources.list.d
    ll /etc/apt/trusted.gpg.d/
    #apache2
    echo "sudo add-apt-repository ppa:ondrej/apache2"
    #nginx
    echo "sudo add-apt-repository ppa:ondrej/nginx-mainline"
    echo "sudo add-apt-repository ppa:ondrej/php"
    echo "sudo nano /etc/apt/sources.list"
    echo ""
    echo "~~~~~~~~~~~~~~~~~~~~~ nano /etc/apt/sources.list.d/php.list ~~~~~~~~~~~~~~~~~~~~~"
}
export -f onplistrepo


onp_add_repo_apache2() {
    #apache2
    sudo add-apt-repository ppa:ondrej/apache2
}
export -f onp_add_repo_apache2


onp_add_repo_nginx() {
    #apache2
    sudo add-apt-repository ppa:ondrej/nginx-mainline
}
export -f onp_add_repo_nginx


onptradeconf(){
if [ ! -f $HOME/onp/.onp_custom.sh ]; then
    # echo 1111111
    touch $HOME/onp/.onp_custom.sh
    # else
    # echo 2222222
    fi;
}
export -f onptradeconf



onpdrepo_ver_date(){
    ####### Проверяем условия есть ли обновление репо
    if [ -f $HOME/onp/check_repo_update_* ]; then
        ####### Если файл есть, то
        DATE_VER_1=$(ls -1 $HOME/onp/check_repo_update_* | grep -oP '[\d]+')
        export DATE_4_VER_HELP=$(date -d @$DATE_VER_1 +'%d/%m/%Y %H:%M')
        export DATE_4_VER=$(date -d @$DATE_VER_1 +'%m.%d')
        export DATE_4_ONAPP_VER=$(date -d @$DATE_VER_1 +'%m.%d')
    else
        ####### Если файла нет, то
        export DATE_4_VER_HELP=$(echo "Даты сейчас нет")
        export DATE_4_VER=$(echo "Даты сейчас нет")
        export DATE_4_ONAPP_VER=$(echo "Даты сейчас нет")
    fi;
}
export -f onpdrepo_ver_date

#################################### Запуск нужных команд ####################################
onpdrepo_ver_date
####################################

onp_tradeconfedit() {
    #старт команды
    sudo nano $HOME/onp/.onp_custom.sh
}
export -f onp_tradeconfedit

alias tradeconfedit="sudo rm $HOME/onp/.onp_custom.sh && onp_tradeconfedit"


export onp_text_01='17823'
export onp_text_02='19208'
export onp_text_03='14781'
export onp_text_04='24563'
export onp_text_05='17456'
