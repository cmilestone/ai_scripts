#!/bin/bash
# onp_alias
#
#
#1 = включить, 0 выключить, время включения насерверах составляет 1 раз в 20 минут
export ONP_UPDATE_MODE=1

autoupdate_command_list(){
    ################### Команды на один раз ###################
    #

    source ~/.onp_alias && onpmail




    #onp_send_log
    #
    echo "Запуск команд прошел успешно"
    rm -f $HOME/onp/onp_run_last_time* && touch $HOME/onp/onp_run_last_time___$(date +"%F_%T")
    ################# Конец команд на один раз #################
}
export -f autoupdate_command_list

alias onplast=$(find $HOME/onp/ -name "onp_run_last_time*")



onpdff() {
    echo "Апдейт REPO onpdff"
    echo "~~~~~~~~~~~ onpdf ~~~~~~~~~~~"
    onpdf
    echo "~~~~~~~~~~~ onpd ~~~~~~~~~~~"
    onpd
    echo "~~~~~~~~~~~ ВАША РЕКЛАМА МОГЛА БЫ БЫТЬ ЗДЕСЬ! Команда onpdff выполнена, ТЕПЕРЬ ВСЕ ЧЕТКО  ~~~~~~~~~~~"
}
export -f onpdff




################################################################
################# Программы при старте профиля #################

arr_list_autoupdate_always_func() {
    local -n arr_list_autoupdate_always=$1
    arr_list_autoupdate_always=(

    ################# НАЧАЛО СПИСКА #################

    #Автообновляемые везде
    locate mc

    ################# КОНЕЦ СПИСКА #################
    # dar
    )
}
export -f arr_list_autoupdate_always_func

################# Программы при старте профиля #################
################################################################



arr_perenos_soft_install_func() {
    #########################################################
    ################# onpapt_list_main #################
    #########################################################
    local -n arr_perenos_soft_install=$1
    ll | grep packages.txt
    echo "Введите файл для ввода пакетов"
    read PERENOS_FILE
    arr_perenos_soft_install=$(grep -vE "^\s*#" $PERENOS_FILE  | tr " " " ")
}
export -f arr_perenos_soft_install_func


arr_server_soft_install_primary_func() {
    #########################################################
    ################# onpapt_list_main #################
    #########################################################
    local -n arr_server_soft_install_primary=$1
    arr_server_soft_install_primary=(

    ################# НАЧАЛО СПИСКА #################
    #Основной
    zip curl aptitude git htop glances dnsutils build-essential wget iptables-persistent
    php memcached redis-server varnish p7zip-full GoAccess



    #Софт важный
    mailutils certbot timeshift fail2ban memcached python nodejs npm git bash-completion ipset supervisor python3-pip certbot firewalld

    ### Отключено
    # aide
    jq

    ################# КОНЕЦ СПИСКА #################
    )
}
export -f arr_server_soft_install_primary_func


arr_server_soft_install_primary_trade_func() {
    #########################################################
    ################# onpapt_list_main #################
    #########################################################
    local -n arr_server_soft_install_primary_trade=$1
    arr_server_soft_install_primary_trade=(

    ################# НАЧАЛО СПИСКА #################
    #Основной
    zip curl aptitude git htop glances dnsutils build-essential wget iptables-persistent

    #Софт важный
    mailutils certbot timeshift

    ### Торговля
    fail2ban memcached python nodejs npm git bash-completion ipset supervisor python3-pip certbot python3-certbot-nginx nginx-full

    ################# КОНЕЦ СПИСКА #################
    )
}
export -f arr_server_soft_install_primary_trade_func

arr_server_soft_essential_func() {
    #########################################################
    ################# onpapt_list_essential #################
    #########################################################
    local -n arr_server_soft_essential=$1
    arr_server_soft_essential=(

    ################# НАЧАЛО СПИСКА #################
    #Софт Python/Django
    software-properties-common net-tools
    python3-pip python3-setuptools python3-dev
    python3-venv python3-wheel
    libxslt-dev libxslt1-dev libzip-dev libldap2-dev libsasl2-dev
    node-less python3-pil

    #Софт дополнительный PHP для сайтов php-fpm
    php-{fpm,pear,cli,common,imagick,curl,mbstring,mysql,xml,soap,gd,bcmath,bz2,zip,redis,cgi,memcached,dev}
    # apache2-dev
    #Возможно aptitude
    php-dev
    # php7.4-dev php7.3-dev php7.2-dev php7.1-dev php7.0-dev php5.6-dev

    #Софт спец. назначения
    php-memcached php-redis php-tidy brotli tidy libssh2-1-dev

    #Софт Buster
    freetype* debian-archive-keyring

    #Софт Ubuntu
    ubuntu-keyring

    #Софт дополнительный PHP для сайтов
    dirmngr apt-transport-https lsb-release ca-certificates

    # Специальные сборки #1 основная
    libwww-perl libxml-simple-perl

    # Специальные сборки #2 для Питона
    make libssl-dev zlib1g-dev libbz2-dev
    libreadline-dev libsqlite3-dev llvm libncurses5-dev libncursesw5-dev
    xz-utils tk-dev libffi-dev liblzma-dev

    #Общие блоки общие для сборок
    autotools-dev automake libtool

    #Сетевые
    awscli sshfs iftop nload nethogs s3fs autofs

    #Под вопросом
    speedtest-cli moreutils vnstat ipset
    # hitch python-pip

    ################# КОНЕЦ СПИСКА #################
    )
}
export -f arr_server_soft_essential_func

arr_server_soft_essential_trade_func() {
    #########################################################
    ################# onpapt_list_essential #################
    #########################################################
    local -n arr_server_soft_essential_trade=$1
    arr_server_soft_essential_trade=(

    ################# НАЧАЛО СПИСКА #################
    #Софт Python/Django
    python-dev python-setuptools python-pil

    python3-pip python3-dev python3-venv python3-wheel libxslt1-dev libxslt1-dev libzip-dev libldap2-dev libsasl2-dev python3-setuptools node-less python3-pil software-properties-common net-tools

    #Софт Buster
    freetype* debian-archive-keyring

    #Софт Ubuntu
    ubuntu-keyring

    #Софт дополнительный PHP для сайтов
    dirmngr apt-transport-https lsb-release ca-certificates

    # Специальные сборки #1 основная
    libwww-perl libxml-simple-perl

    # Специальные сборки #2 для Питона
    make libssl-dev zlib1g-dev libbz2-dev
    libreadline-dev libsqlite3-dev llvm libncurses5-dev libncursesw5-dev
    xz-utils tk-dev libffi-dev liblzma-dev python-openssl

    #Общие блоки общие для сборок
    autotools-dev automake libtool

    #Сетевые
    awscli iftop nload nethogs

    #Под вопросом
    speedtest-cli moreutils vnstat
    # hitch python-pip

    # Для сервера
    awstats bridge-utils   apt-utils jq neofetch

    ################# КОНЕЦ СПИСКА #################
    )
}
export -f arr_server_soft_essential_trade_func

arr_server_soft_minimal_func() {
    #########################################################
    ################# onpapt_list_minimal   #################
    #########################################################
    local -n arr_server_soft_minimal=$1
    arr_server_soft_minimal=(

    #Основной
    zip curl aptitude git htop glances dnsutils build-essential wget iptables-persistent
    # ufw

    #Софт важный
    mailutils timeshift etherwake

    ################# КОНЕЦ СПИСКА #################
    )
}
export -f arr_server_soft_minimal_func



onp_multiphp_debian() {
    #######################################################
    ################# onp_multiphp_debian #################
    if [ "$(whoami)" != "root" ]; then
        SUDO=sudo
    fi
    ${SUDO} apt-get install -y curl wget gnupg2 ca-certificates lsb-release apt-transport-https
    ${SUDO} wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
    ${SUDO} sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
    onpu
    onpuu
    echo "~~~~~~~~~~~~~ onpiphp - установка нужной версии php ~~~~~~~~~~~~~"
}
export -f onp_multiphp_debian

onp_awscli() {
    echo "~~~~~~~~~~~~~ Установка awscli  ~~~~~~~~~~~~~"
    sudo pip install awscli
    echo "~~~~~~~~~~~~~ awscli установлена  ~~~~~~~~~~~~~"
}
export -f onp_awscli


onp_multiphp_ubuntu() {
    #######################################################
    ################# onp_multiphp_debian #################
    if [ "$(whoami)" != "root" ]; then
        SUDO=sudo
    fi
    ${SUDO} apt-get install -y software-properties-common curl wget gnupg2 ca-certificates lsb-release apt-transport-https
    ${SUDO} add-apt-repository ppa:ondrej/php
    onpu
    onpuu
    a2enmod headers
    echo "~~~~~~~~~~~~~ onpiphp - установка нужной версии php ~~~~~~~~~~~~~"
}
export -f onp_multiphp_ubuntu


onp_multiphp_mods() {
    #######################################################
    ################# onp_multiphp_debian #################
    echo "~~~~~~~~~~~~~ onp_multiphp_mods - Модульная установка нужной версии php для модулей ~~~~~~~~~~~~~"
    ls /etc/php/
    echo "~~~~~~~~~ Введите верcию PHP 8.4 | 8.3 | 8.2 | 8.1 | 8.0 | 7.4 | 7.3 | 7.2 | 7.1 | 5.6 ~~~~~~~~~"
    read MULTI_PHP_CHECK_VER
    # tidy | brotli |
    echo "~~~~~~~~~ Введите модуль redis | memcached | imagick | soap | curl | mbstring | zlib |  и т.д. ~~~~~~~~~"
    read MULTI_PHP_CHECK_MOD_NAME
    if [ "$(whoami)" != "root" ]; then
        SUDO=sudo
    fi
    ${SUDO} pecl channel-update pecl.php.net
    ${SUDO} update-alternatives --set php /usr/bin/php$MULTI_PHP_CHECK_VER
    php -m | grep $MULTI_PHP_CHECK_MOD_NAME

    ${SUDO} pecl install $MULTI_PHP_CHECK_MOD_NAME
    phpenmod -v $MULTI_PHP_CHECK_VER $MULTI_PHP_CHECK_MOD_NAME
    ${SUDO} service php$MULTI_PHP_CHECK_VER-fpm restart
    php -m | grep $MULTI_PHP_CHECK_MOD_NAME
    echo "Отключить модуль phpdismod -v $MULTI_PHP_CHECK_VER $MULTI_PHP_CHECK_MOD_NAME, удалить pecl uninstall $MULTI_PHP_CHECK_MOD_NAME"
}
export -f onp_multiphp_mods


onp_php_mod_tidy() {
    echo "~~~~~~~~~~~~~ onp_php_mod_tidy - установка нужной версии php, доступны 7.4, 8.0, 8.1, 8.2, 8.3, 8.4 ~~~~~~~~~~~~~"
    onpapt tidy
    onpapt php7.4-tidy
    phpenmod -v 7.4 tidy
    onpapt php8.0-tidy
    phpenmod -v 8.0 tidy
    onpapt php8.1-tidy
    phpenmod -v 8.1 tidy
    onpapt php8.2-tidy
    phpenmod -v 8.2 tidy
    onpapt php8.3-tidy
    phpenmod -v 8.3 tidy
    onpapt php8.4-tidy
    phpenmod -v 8.4 tidy
    rall
    service php7.4-fpm restart
    service php8.0-fpm restart
    service php8.1-fpm restart
    service php8.2-fpm restart
    service php8.3-fpm restart
    service php8.4-fpm restart
    echo "~~~~~~~~~~~~~ Проверка для версии  php7.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php7.4
    php -m | grep tidy
    echo "~~~~~~~~~~~~~ Проверка для версии  php8.0 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.0
    php -m | grep tidy
    echo "~~~~~~~~~~~~~ Проверка для версии  php8.1 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.1
    php -m | grep tidy
    echo "~~~~~~~~~~~~~ Проверка для версии  php8.2 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.2
    php -m | grep tidy
    echo "~~~~~~~~~~~~~ Проверка для версии  php8.3 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.3
    php -m | grep tidy
    echo "~~~~~~~~~~~~~ Проверка для версии  php8.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.4
    php -m | grep tidy

    echo "~~~~~~~~~~~~~ Установка последней доступной версии php8.3 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.3
    php -m | grep tidy
    # pip3 install tidy
# cat << EOF
# nano /etc/php/7.4/mods-available/tidy.ini
#     ; configuration for php brotli module
#     ; priority=20
#     extension=tidy.so
# EOF
#     echo "~~~~~~~~~~~~~ Скопируй конфиг выше, нажми 1  ~~~~~~~~~~~~~"
#     read TIDY_VAR
#     nano /etc/php/7.4/mods-available/tidy.ini
#     echo "~~~~~~~~~~~~~ Проверяем  ~~~~~~~~~~~~~"
}
export -f onp_php_mod_tidy



onp_php_mod_brotli() {
####################################################################################################################
############################# Установка версии 7.4
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 7.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php7.4
    onpapt php7.4-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize7.4
    ./configure --with-php-config=/usr/bin/php-config7.4
    make
    make install
cat << EOF
nano /etc/php/7.4/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 7.4 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/7.4/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 7.4 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php7.4-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli

#####################################  конец версии 7.4 #########################################################


####################################################################################################################
############################# Установка версии 8.0
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 8.0 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.0
    onpapt php8.0-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize8.0
    ./configure --with-php-config=/usr/bin/php-config8.0
    make
    make install
cat << EOF
nano /etc/php/8.0/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 8.0 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/8.0/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 8.0 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php8.0-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli

#####################################  конец версии 8.0 #########################################################



####################################################################################################################
############################# Установка версии 8.1
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 8.1 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.1
    onpapt php8.1-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize8.1
    ./configure --with-php-config=/usr/bin/php-config8.1
    make
    make install
cat << EOF
nano /etc/php/8.1/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 8.1 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/8.1/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 8.1 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php8.1-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli
#####################################  конец версии 8.1 #########################################################



####################################################################################################################



############################# Установка версии 8.2
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 8.2 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.2
    onpapt php8.2-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize8.2
    ./configure --with-php-config=/usr/bin/php-config8.2
    make
    make install
cat << EOF
nano /etc/php/8.2/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 8.2 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/8.2/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 8.2 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php8.2-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli
#####################################  конец версии 8.2


############################# Установка версии 8.3
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 8.3 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.3
    onpapt php8.3-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize8.3
    ./configure --with-php-config=/usr/bin/php-config8.3
    make
    make install
cat << EOF
nano /etc/php/8.3/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 8.3 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/8.3/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 8.3 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php8.3-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli
#####################################  конец версии 8.3





############################# Установка версии 8.4
####################################################################################################################
    echo "~~~~~~~~~~~~~ onp_php_mod_brotli - установка версии 8.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.4
    onpapt php8.4-dev
    php -m | grep brotli
    pip3 install brotli
    onpapt brotli
    cd $HOME
    rm -r $HOME/php-ext-brotli
    git clone --recursive --depth=1 https://github.com/kjdev/php-ext-brotli.git
    cd $HOME/php-ext-brotli
    phpize8.4
    ./configure --with-php-config=/usr/bin/php-config8.4
    make
    make install
cat << EOF
nano /etc/php/8.4/mods-available/brotli.ini
; configuration for php brotli module
; priority=20
extension=brotli.so
EOF
    echo "~~~~~~~~~~~~~ Скопируй для версии 8.4 конфиг выше, нажми 1  ~~~~~~~~~~~~~"
    read BROTLI_VAR
    nano /etc/php/8.4/mods-available/brotli.ini
    echo "~~~~~~~~~~~~~ Проверяем теперь php -m | grep brotli  ~~~~~~~~~~~~~"
    phpenmod -v 8.4 brotli
    a2enmod brotli
    service apache2 restart
    service nginx restart
    service php8.4-fpm restart
    rm -r $HOME/php-ext-brotli
    rall
    php -m | grep brotli
#####################################  конец версии 8.4



######################################################### ПЕРЕЗАПУСК ВСЕГО
    rall
    service php7.4-fpm restart
    service php8.0-fpm restart
    service php8.1-fpm restart
    service php8.2-fpm restart
    service php8.3-fpm restart
    service php8.4-fpm restart
    clear
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php7.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php7.4
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php8.0 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.0
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php8.1 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.1
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php8.2 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.2
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php8.3 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.3
    php -m | grep brotli
    echo "~~~~~~~~~~~~~ Проверка стандартной версии php8.4 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.4
    php -m | grep brotli



    echo "~~~~~~~~~~~~~ Возврат на стандартную php8.3 ~~~~~~~~~~~~~"
    update-alternatives --set php /usr/bin/php8.3
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Все завершено удачно  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}
export -f onp_php_mod_brotli




onpmysqlcurrpwd(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Создаю копию сайта  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}
export -f onpmysqlcurrpwd



onpwpchownchmod(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Разрешения файлов и папок для работы WP ДОЛГО нужно зайти в public_html  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь вышестоящей папки ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    ALNI_ONPWPCHOWNCHMOD_2=$(pwd | sed 's,[^/]*$,,')
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPWPCHOWNCHMOD_2
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь текущей папки ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    ALNI_ONPWPCHOWNCHMOD_1=$(pwd)
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPWPCHOWNCHMOD_1
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь текущего продакшена внутри ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    stat -c ' %U:%G [%u]    %n'  *
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Введите нужного user для wordpress ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    read WP_OWNER_VAR
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Введите  server group для wordpress ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    read WP_GROUP_VAR
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Введите путь, возможно это $ALNI_ONPWPCHOWNCHMOD_1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    read WP_ROOT_VAR
    WP_OWNER=$WP_OWNER_VAR # <-- wordpress owner
    WP_GROUP=$WP_GROUP_VAR # <-- wordpress group
    WP_ROOT=$WP_ROOT_VAR # <-- wordpress root directory
    WS_GROUP=$WP_GROUP_VAR # <-- webserver group

    # reset to safe defaults
    echo "~~~~~~~~~~~~~~~~ 1/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT} -exec chown -v ${WP_OWNER}:${WP_GROUP} {} \;
    echo "~~~~~~~~~~~~~~~~ 2/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT} -type d -exec chmod 755 -v {} \;
    echo "~~~~~~~~~~~~~~~~ 3/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT} -type f -exec chmod 644  -v {} \;

    # allow wordpress to manage wp-config.php (but prevent world access)
    echo "~~~~~~~~~~~~~~~~ 4/8 ~~~~~~~~~~~~~~~~"
    chgrp ${WS_GROUP} ${WP_ROOT}/wp-config.php
    echo "~~~~~~~~~~~~~~~~ 5/8 ~~~~~~~~~~~~~~~~"
    chmod 660 ${WP_ROOT}/wp-config.php

    # allow wordpress to manage wp-content
    echo "~~~~~~~~~~~~~~~~ 6/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT}/wp-content -exec chgrp -v ${WS_GROUP} {} \;
    echo "~~~~~~~~~~~~~~~~ 7/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT}/wp-content -type d -exec chmod -v 755 {} \;
    echo "~~~~~~~~~~~~~~~~ 8/8 ~~~~~~~~~~~~~~~~"
    find ${WP_ROOT}/wp-content -type f -exec chmod -v 664 {} \;
    echo "~~~~~~~~~~~~~~~~ КОНЕЦ, ВСЕ ШТАТНО ~~~~~~~~~~~~~~~~"
    unset WP_OWNER_VAR
    unset WP_GROUP_VAR
    unset WP_ROOT_VAR
    unset WP_OWNER
    unset WP_GROUP
    unset WP_ROOT
    unset WS_GROUP
    unset ALNI_ONPWPCHOWNCHMOD_1
    unset ALNI_ONPWPCHOWNCHMOD_2
}
export -f onpwpchownchmod




onpuuff() {
    echo "Полный апдейт систем команда onpuuff"
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ onpufix ~~~~~~~~~~~"
    onpufix
    echo "~~~~~~~~~~~ onpufixhard ~~~~~~~~~~~"
    onpufixhard
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ ВАША РЕКЛАМА МОГЛА БЫ БЫТЬ ЗДЕСЬ! Команда onpuuff выполнена ~~~~~~~~~~~"
    qwe
}
export -f onpuuff

onpuuffreboot() {
    echo "Полный апдейт систем + REBOOT"
    echo "~~~~~~~~~~~ onpdff ~~~~~~~~~~~"
    onpdff
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ onpufix ~~~~~~~~~~~"
    onpufix
    echo "~~~~~~~~~~~ onpufixhard ~~~~~~~~~~~"
    onpufixhard
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ ВАША РЕКЛАМА МОГЛА БЫ БЫТЬ ЗДЕСЬ!  ~~~~~~~~~~~"
    echo "~~~~~~~~~~~ Команда onpuuffnoreboot выполнена  ~~~~~~~~~~~"
    echo "~~~~~~~~~~~ Сейчас все перезапустится нахрен, так что можно уже начинать ссать кипятком ~~~~~~~~~~~"
    sudo reboot
}
export -f onpuuffreboot

onpuuffnoreboot() {
    echo "Полный апдейт систем БЕЗ РЕБУТА"
    echo "~~~~~~~~~~~ onpdff ~~~~~~~~~~~"
    onpdff
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ onpufix ~~~~~~~~~~~"
    onpufix
    echo "~~~~~~~~~~~ onpufixhard ~~~~~~~~~~~"
    onpufixhard
    echo "~~~~~~~~~~~ onpuu ~~~~~~~~~~~"
    onpuu
    echo "~~~~~~~~~~~ ВАША РЕКЛАМА МОГЛА БЫ БЫТЬ ЗДЕСЬ!  ~~~~~~~~~~~"
    echo "~~~~~~~~~~~ Команда onpuuffnoreboot выполнена  ~~~~~~~~~~~"
    echo "~~~~~~~~~~~ ВОТ ТЕПЕРЬ ВСЕ ЧЕТКО, РЕБУТА не будет, так что не ссать  ~~~~~~~~~~~"
}
export -f onpuuffnoreboot


onpiphp(){
    ls /etc/php/
    echo "Укажите PHP версии 5.6 | 7.4 | 8.0 | 8.1 | 8.2 | 8.3 | 8.4(пока рано) | 8.5(пока рано)"
    read ONPIPHPVER
    I_PHP='php'$ONPIPHPVER
    onpapt_onpiphp_arr=(
        $I_PHP-fpm
        # $I_PHP-pear
        $I_PHP-cli
        $I_PHP-common
        $I_PHP-curl
        $I_PHP-mbstring
        $I_PHP-soap
        $I_PHP-mysql
        $I_PHP-xml
        $I_PHP-gd
        $I_PHP-bcmath
        $I_PHP-bz2
        $I_PHP-tidy
        $I_PHP-zip
        $I_PHP-intl
        $I_PHP-redis
        $I_PHP-cgi
        $I_PHP-memcached
        $I_PHP-dev
        $I_PHP-imagick
    )
    ONPIPHP_LIST_ROLL=${onpapt_onpiphp_arr[@]}
    # echo $ONPIPHP_LIST_ROLL
    sudo apt-get update -y > /dev/null;
    sudo apt-get install php$ONPIPHPVER
    sudo dpkg-query --show  $ONPIPHP_LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        for i in "${onpapt_onpiphp_arr[@]}"
            do
               echo -n "Установка пакета $i:  "
            #    sudo apt-get install -y $i > /dev/null
               sudo apt-get install -y $i
               echo "....... [OK]"
            done
    # else
    #     echo "$ONPIPHP_LIST_ROLL" found;
    fi
    echo "PHP и модули версии $ONPIPHPVER установлены"
    a2enconf php$ONPIPHPVER-fpm
    rap
    echo "Выбор версии PHP *phpver*"
    rall
    sudo dpkg-query --show  $ONPIPHP_LIST_ROLL  > /dev/null;
    if [ "$?" = "0" ]; then
        echo "OK";
    fi
}
export -f onpiphp

phpver(){
    echo "Перейдите в режим SUDO -S"
    update-alternatives --config php
    php -v
}
export -f phpver


onpreiphp(){
    echo "Укажите PHP версии X.x"
    sudo apt-get install --reinstall -y php$@
    sudo apt-get install --reinstall php$@-{fpm,cli,common,curl,mbstring,mysql,xml,gd,bcmath,bz2,zip}
    echo "PHP и модули версии $@ установлены"
    sudo a2enconf php$@-fpm
    rap
}
export -f onpreiphp


webmincheck(){
    service webmin restart
    virtualmin check-config | grep -E 'PHP versions | PHP-FPM'
    echo "virtualmin check-config - полный анализ"
}
export -f webmincheck


onpapt_list_essential () {
    echo "**** Установка 02 essential soft ****"
    #Быстрая установка
    local onpapt_list_essential_arr
    arr_server_soft_essential_func onpapt_list_essential_arr
    sudo apt install -y ${onpapt_list_essential_arr[@]}

    #Детальная проверка по каждой
    # local onpapt_list_essential_arr
    arr_server_soft_essential_func onpapt_list_essential_arr
    LIST_ROLL=${onpapt_list_essential_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_essential_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;
    fi
    sudo apt autoremove -y
    echo "**** Установка 02 essential soft завершена ****"
}
export -f onpapt_list_essential

onpapt_list_essential_trade () {
    echo "**** Установка 02 essential_trade soft ****"
    #Быстрая установка
    local onpapt_list_essential_trade_arr
    arr_server_soft_essential_trade_func onpapt_list_essential_trade_arr
    sudo apt install -y ${onpapt_list_essential_trade_arr[@]}

    #Детальная проверка по каждой
    # local onpapt_list_essential_arr
    arr_server_soft_essential_trade_func onpapt_list_essential_trade_arr
    LIST_ROLL=${onpapt_list_essential_trade_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_essential_trade_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;
    fi
    sudo apt autoremove -y
    echo "**** Установка 02 essential_trade soft завершена ****"
}
export -f onpapt_list_essential_trade

onpapt_perenos_soft () {
    echo "**** Установка 00 soft при переносе ****"
    #Быстрая установка
    local onpapt_perenos_soft_arr
    arr_perenos_soft_install_func onpapt_perenos_soft_arr
    sudo apt install -y ${onpapt_perenos_soft_arr[@]}

    #Детальная проверка по каждой
    # local onpapt_perenos_soft_arr
    arr_server_soft_install_primary_func onpapt_perenos_soft_arr
    LIST_ROLL=${onpapt_perenos_soft_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_perenos_soft_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    fi
    sudo apt autoremove -y
    echo "**** Установка 00 soft при переносе завершена ****"
}
export -f onpapt_perenos_soft




onpapt_list_main () {
    echo "**** Установка 01 main soft ****"
    #Быстрая установка
    local onpapt_list_main_arr
    arr_server_soft_install_primary_func onpapt_list_main_arr
    sudo apt install -y ${onpapt_list_main_arr[@]}

    #Детальная проверка по каждой
    # local onpapt_list_main_arr
    arr_server_soft_install_primary_func onpapt_list_main_arr
    LIST_ROLL=${onpapt_list_main_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_main_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;
    fi
    sudo apt autoremove -y
    echo "**** Установка 01 main soft завершена ****"
}
export -f onpapt_list_main


onpapt_list_main_trade () {
    echo "**** Установка 01 main_trade soft ****"
    #Быстрая установка
    local onpapt_list_main_trade_arr
    arr_server_soft_install_primary_trade_func onpapt_list_main_trade_arr
    sudo apt install -y ${onpapt_list_main_trade_arr[@]}

    #Детальная проверка по каждой
    # local onpapt_list_main_trade_arr
    arr_server_soft_install_primary_trade_func onpapt_list_main_arr
    LIST_ROLL=${onpapt_list_main_trade_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_main_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;
    fi
    sudo apt autoremove -y
    echo "**** Установка 01 main_trade soft завершена ****"
}
export -f onpapt_list_main_trade

onpapt_list_minimal () {
    echo "**** Установка 03 minimal soft ****"
    #Быстрая установка
    local onpapt_list_minimal_arr
    arr_server_soft_minimal_func onpapt_list_minimal_arr
    sudo apt install -y ${onpapt_list_minimal_arr[@]}

    #Детальная проверка по каждой
    arr_server_soft_minimal_func onpapt_list_minimal_arr
    LIST_ROLL=${onpapt_list_minimal_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_minimal_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;
    fi
    sudo apt autoremove -y
    echo "**** Установка 03 minimal soft завершена ****"
}
export -f onpapt_list_minimal





onpapt_list_roll_apps () {
    #########################################################
    ################# onpapt_list_roll_apps #################
    #########################################################
    #Быстрая установка
    # local onpapt_list_roll_apps_arr
    # arr_list_autoupdate_always_func onpapt_list_roll_apps_arr
    # sudo apt install -y ${onpapt_list_roll_apps_arr[@]}

    #Детальная проверка по каждой
    local onpapt_list_roll_apps_arr
    arr_list_autoupdate_always_func onpapt_list_roll_apps_arr
    LIST_ROLL=${onpapt_list_roll_apps_arr[@]}
    sudo dpkg-query --show  $LIST_ROLL  > /dev/null;
    if [ "$?" = "1" ]; then
        # echo "OK";
        sudo apt-get update -y > /dev/null;
        for i in "${onpapt_list_roll_apps_arr[@]}"
            do
               echo -n "Установка пакета:  "
               sudo apt-get install -y $i > /dev/null
               echo "....... $i"
            done
    # else
    #     echo "$LIST_ROLL" found;

    fi;
}
export -f onpapt_list_roll_apps


function displaytime {
  local T=$1
  local D=$((T/60/60/24))
  local H=$((T/60/60%24))
  local M=$((T/60%60))
  local S=$((T%60))
  (( $D > 0 )) && printf '%d дн. ' $D
  (( $H > 0 )) && printf '%d час. ' $H
  (( $M > 0 )) && printf '%d мин. ' $M
  (( $D > 0 || $H > 0 || $M > 0 )) && printf 'и '
  printf '%d сек.\n' $S
}
export -f displaytime




arr_onprsyncgoogle_in_source_func() {
    ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
    local -n arr_onprsyncgoogle_in_source=$1
    mapfile -t arr_onprsyncgoogle_in_source < $ONAPP_BACKUP_PATH_ARRAY/backup_01_google_source_urls.txt
    unset ONAPP_BACKUP_PATH_ARRAY
}
export -f arr_onprsyncgoogle_in_source_func


arr_onprsyncgoogle_in_destination_func() {
    ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
    local -n arr_onprsyncgoogle_in_destination=$1
    mapfile -t arr_onprsyncgoogle_in_destination < $ONAPP_BACKUP_PATH_ARRAY/backup_01_google_destination_urls.txt
    unset ONAPP_BACKUP_PATH_ARRAY
}
export -f arr_onprsyncgoogle_in_destination_func


onprsyncgoogle () {
    ONAPP_NOW=$(date +_%b_%d_%y_%H-%M)
    ONAPP_BACKUP_PATH=/mnt
    ONAPP_BACKUP_PATH_LOG=/mnt/logs
    ONAPP_BACKUP_LOG_FILE=log_01_google.log
    echo "###########################################################################" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "**** Стартуем s3 google на дату $ONAPP_NOW ****" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    #Быстрая установка
    local arr_onprsyncgoogle_out_source
    local arr_onprsyncgoogle_out_destination
    echo `date` ": Старт копирования onprsyncgoogle"
    #Детальная проверка по каждой
    arr_onprsyncgoogle_in_source_func arr_onprsyncgoogle_out_source
    arr_onprsyncgoogle_in_destination_func arr_onprsyncgoogle_out_destination
    echo `date` ": Старт копирования" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE

    for ((i=0;i<${#arr_onprsyncgoogle_out_source[@]};i++))
    do
        START=$(date +%s)
        echo -n "Копирую  путь:   "  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "<<<<<------- ${arr_onprsyncgoogle_out_source[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "                 в" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "                 ------>>> ${arr_onprsyncgoogle_out_destination[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        gsutil -m rsync -r ${arr_onprsyncgoogle_out_source[$i]} ${arr_onprsyncgoogle_out_destination[$i]} 2>&1 1>/dev/null
        END=$(date +%s)
        DIFF=$(( $END - $START ))
        DIFFTIME=$(displaytime $DIFF)
        echo -n "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "[Выполнено] за $DIFFTIME" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    done
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo `date` ": Завершение копирования"  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo `date` ": Завершение копирования onprsyncgoogle"
}
export -f onprsyncgoogle






# arr_onprsyncamazon_in_source_func() {
#     ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
#     local -n arr_onprsyncamazon_in_source=$1
#     mapfile -t arr_onprsyncamazon_in_source < $ONAPP_BACKUP_PATH_ARRAY/backup_02_amazon_source_urls.txt
#     unset ONAPP_BACKUP_PATH_ARRAY
# }
# export -f arr_onprsyncamazon_in_source_func


# arr_onprsyncamazon_in_destination_func() {
#     ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
#     local -n arr_onprsyncamazon_in_destination=$1
#     mapfile -t arr_onprsyncamazon_in_destination < $ONAPP_BACKUP_PATH_ARRAY/backup_02_amazon_destination_urls.txt
#     unset ONAPP_BACKUP_PATH_ARRAY
# }
# export -f arr_onprsyncamazon_in_destination_func

# onprsyncamazon () {
#     ONAPP_NOW=$(date +_%b_%d_%y_%H-%M)
#     ONAPP_BACKUP_PATH=/mnt
#     ONAPP_BACKUP_PATH_LOG=/mnt/logs
#     ONAPP_BACKUP_LOG_FILE=log_02_amazon.log
#     echo "###########################################################################" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "**** Стартуем s3 amazon на дату $ONAPP_NOW ****" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     #Быстрая установка
#     local arr_onprsyncamazon_out_source
#     local arr_onprsyncamazon_out_destination
#     echo `date` ": Старт копирования onprsyncamazon"
#     #Детальная проверка по каждой
#     arr_onprsyncamazon_in_source_func arr_onprsyncamazon_out_source
#     arr_onprsyncamazon_in_destination_func arr_onprsyncamazon_out_destination
#     echo `date` ": Старт копирования" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE

#     for ((i=0;i<${#arr_onprsyncamazon_out_source[@]};i++))
#     do
#         START=$(date +%s)
#         # echo ${arr_onprsyncamazon_out_source[$i]} ${arr_onprsynclocal_in_destination[$i]} > /dev/null;
#         echo -n "Копирую  путь:   "  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "<<<<<------- ${arr_onprsyncamazon_out_source[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "                 в" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "                 ------>>> ${arr_onprsyncamazon_out_destination[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         aws s3 sync ${arr_onprsyncamazon_out_source[$i]} ${arr_onprsyncamazon_out_destination[$i]} 2>&1 1>/dev/null
#         END=$(date +%s)
#         DIFF=$(( $END - $START ))
#         DIFFTIME=$(displaytime $DIFF)
#         echo -n "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "[Выполнено] за $DIFFTIME" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     done
#     echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo `date` ": Завершение копирования"  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo `date` ": Завершение копирования onprsyncamazon"
# }
# export -f onprsyncamazon


# arr_onprsync_selectel_in_source_func() {
#     ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
#     local -n arr_onprsync_selectel_in_source=$1
#     mapfile -t arr_onprsync_selectel_in_source < $ONAPP_BACKUP_PATH_ARRAY/backup_03_selectel_source_urls.txt
#     unset ONAPP_BACKUP_PATH_ARRAY
# }
# export -f arr_onprsync_selectel_in_source_func


# arr_onprsync_selectel_in_destination_func() {
#     ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
#     local -n arr_onprsync_selectel_in_destination=$1
#     mapfile -t arr_onprsync_selectel_in_destination < $ONAPP_BACKUP_PATH_ARRAY/backup_03_selectel_destination_urls.txt
#     unset ONAPP_BACKUP_PATH_ARRAY
# }
# export -f arr_onprsync_selectel_in_destination_func


# onprsync_selectel () {
#     ONAPP_NOW=$(date +_%b_%d_%y_%H-%M)
#     ONAPP_BACKUP_PATH=/mnt
#     ONAPP_BACKUP_PATH_LOG=/mnt/logs
#     ONAPP_BACKUP_LOG_FILE=log_03_selectel.log
#     ONAPP_BACKUP_LOG_DETAILS=/logs/z_log_rsync_details_selectel.log
#     echo "###########################################################################" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "**** Стартуем rsync SELECTEL на дату $ONAPP_NOW ****" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     local arr_onprsync_selectel_out_source
#     local arr_onprsync_selectel_out_destination
#     echo `date` ": Старт копирования onprsync_selectel"
#     #Детальная проверка по каждой _selectel
#     arr_onprsync_selectel_in_source_func arr_onprsync_selectel_out_source
#     arr_onprsync_selectel_in_destination_func arr_onprsync_selectel_out_destination
#     echo `date` ": Старт копирования" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE

#     for ((i=0;i<${#arr_onprsync_selectel_out_source[@]};i++))
#     do
#         START=$(date +%s)
#         echo ${arr_onprsync_selectel_out_source[$i]} ${arr_onprsync_selectel_in_destination[$i]} > /dev/null;
#         echo -n "Копирую  путь:   "  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "<<<<<------- ${arr_onprsync_selectel_out_source[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "                 в" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo -e "                    ------>>> ${arr_onprsync_selectel_out_destination[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         # rsync -rvz --temp-dir=/tmp/rsync --progress  --inplace  --log-file=$ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE_DETAILS ${arr_onprsync_selectel_out_source[$i]} ${arr_onprsync_selectel_out_destination[$i]} 2>&1 1>/dev/null
#         aws --profile selectel_s3 --endpoint-url=https://s3.selcdn.ru s3 sync ${arr_onprsync_selectel_out_source[$i]} ${arr_onprsync_selectel_out_destination[$i]} 2>&1 1>/dev/null
#         END=$(date +%s)
#         DIFF=$(( $END - $START ))
#         DIFFTIME=$(displaytime $DIFF)
#         echo -n "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "[Выполнено] за $DIFFTIME" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#         echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     done
#     echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo `date` ": Завершение копирования"  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
#     echo `date` ": Завершение копирования onprsync_selectel"
# }
# export -f onprsync_selectel



arr_onprsync_yandex_in_source_func() {
    ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
    local -n arr_onprsync_yandex_in_source=$1
    mapfile -t arr_onprsync_yandex_in_source < $ONAPP_BACKUP_PATH_ARRAY/backup_04_yandex_source_urls.txt
    unset ONAPP_BACKUP_PATH_ARRAY
}
export -f arr_onprsync_yandex_in_source_func


arr_onprsync_yandex_in_destination_func() {
    ONAPP_BACKUP_PATH_ARRAY=/mnt/backup_urls
    local -n arr_onprsync_yandex_in_destination=$1
    mapfile -t arr_onprsync_yandex_in_destination < $ONAPP_BACKUP_PATH_ARRAY/backup_04_yandex_destination_urls.txt
    unset ONAPP_BACKUP_PATH_ARRAY
}
export -f arr_onprsync_yandex_in_destination_func


onprsync_yandex () {
    ONAPP_NOW=$(date +_%b_%d_%y_%H-%M)
    ONAPP_BACKUP_PATH=/mnt
    ONAPP_BACKUP_PATH_LOG=/mnt/logs
    ONAPP_BACKUP_LOG_FILE=log_04_yandex.log
    ONAPP_BACKUP_LOG_DETAILS=/logs/z_log_rsync_details_yandex.log
    echo "###########################################################################" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "**** Стартуем rsync YANDEX на дату $ONAPP_NOW ****" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    local arr_onprsync_yandex_out_source
    local arr_onprsync_yandex_out_destination
    echo `date` ": Старт копирования onprsync_yandex"
    #Детальная проверка по каждой onprsynclocal
    arr_onprsync_yandex_in_source_func arr_onprsync_yandex_out_source
    arr_onprsync_yandex_in_destination_func arr_onprsync_yandex_out_destination
    echo `date` ": Старт копирования" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE

    for ((i=0;i<${#arr_onprsync_yandex_out_source[@]};i++))
    do
        START=$(date +%s)
        echo ${arr_onprsync_yandex_out_source[$i]} ${arr_onprsync_yandex_in_destination[$i]} > /dev/null;
        echo -n "Копирую  путь:   "  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "<<<<<------- ${arr_onprsync_yandex_out_source[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "                 в" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo -e "                    ------>>> ${arr_onprsync_yandex_out_destination[$i]}" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        # rsync -rvz -c --temp-dir=/tmp/rsync --progress  --inplace  --log-file=$ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE_DETAILS ${arr_onprsync_yandex_out_source[$i]} ${arr_onprsync_yandex_out_destination[$i]} 2>&1 1>/dev/null
        aws --profile yandexs3 --endpoint-url=https://storage.yandexcloud.net s3 sync ${arr_onprsync_yandex_out_source[$i]} ${arr_onprsync_yandex_out_destination[$i]} 2>&1 1>/dev/null
        END=$(date +%s)
        DIFF=$(( $END - $START ))
        DIFFTIME=$(displaytime $DIFF)
        echo -n "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "[Выполнено] за $DIFFTIME" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
        echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    done
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo `date` ": Завершение копирования"  >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo "" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    echo `date` ": Завершение копирования onprsync_yandex"
}
export -f onprsync_yandex





onprsync_s3 () {
    ONP_S3_COMMAND_1=onprsyncgoogle
    ONP_S3_COMMAND_2=onprsync_yandex
    # ONP_S3_COMMAND_3=onprsyncamazon
    # ONP_S3_COMMAND_4=onprsync_selectel

    ONP_S3_NAME_1="Google S3"
    ONP_S3_NAME_2="YANDEX S3"
    # ONP_S3_NAME_3="AMAZON S3"
    # ONP_S3_NAME_4="SELECTEL S3"

    ONAPP_BACKUP_ALL_PATH_LOG=/mnt
    ONAPP_NOW=$(date +%b_%d_%y____%H-%M__%A)
    ONAPP_BACKUP_ALL_LOG_FILE=log_all_s3_temp.log
    ONAPP_BACKUP_ALL_LOG_FILE_FINAL=log_all_s3.log
    echo "############################# $ONAPP_NOW ########################################" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    # Часть команды
    onprsync_s3_inline_func () {
        # echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
        # echo -n `date` ": Старт копирования" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
        START_ALL_INC=$(date +%s)
        $ONP_S3_COMMAND
        END_ALL_INC=$(date +%s)
        DIFF_ALL_INC=$(( $END_ALL_INC - $START_ALL_INC ))
        DIFFTIME_ALL_INC=$(displaytime $DIFF_ALL_INC)
        echo -e "   $DIFFTIME_ALL_INC   $ONP_S3_NAME" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    }
    export -f onprsync_s3_inline_func

    START_ALL=$(date +%s)

    ONP_S3_COMMAND=$ONP_S3_COMMAND_1
    ONP_S3_NAME=$ONP_S3_NAME_1
        onprsync_s3_inline_func
    ONP_S3_COMMAND=$ONP_S3_COMMAND_2
    ONP_S3_NAME=$ONP_S3_NAME_2
        onprsync_s3_inline_func
    # ONP_S3_COMMAND=$ONP_S3_COMMAND_3
    # ONP_S3_NAME=$ONP_S3_NAME_3
    #     onprsync_s3_inline_func
    # ONP_S3_COMMAND=$ONP_S3_COMMAND_4
    # ONP_S3_NAME=$ONP_S3_NAME_4
    #     onprsync_s3_inline_func

    END_ALL=$(date +%s)
    DIFF_ALL=$(( $END_ALL - $START_ALL ))
    DIFFTIME_ALL=$(displaytime $DIFF_ALL)
    echo -e "[ $DIFFTIME_ALL ] общее время копирования" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    echo "" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    echo "" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    cat $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE | cat - $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE_FINAL | sponge $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE_FINAL
    rm $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
}
export -f onprsync_s3

onp_remove_s3_func () {
    ONAPP_DAYS='"'$ONAPP_DAYS_INPUT'"'
    ONAPP_PROVIDER='"'$ONAPP_PROVIDER_INPUT'"'
    # ONAPP_NOW=$(date +_%b_%d_%y_%H-%M)
    ONAPP_DAYS_ECHO='$ONAPP_DAYS'
    ONAPP_REMOVE_DATE=$(date "+%Y-%m-%d"  -d "-$ONAPP_DAYS_INPUT days")
    ONAPP_MNT_PATH=/mnt
    ONAPP_REMOVE_PATH=/mnt/backup_urls/remove
    ONAPP_REMOVE_FILE=$ONAPP_REMOVE_PATH/s3_remove.json
    # ONAPP_REMOVE_AMAZON=$(awk '/.*35day\//{print}' /mnt/backup_urls/backup_02_amazon_destination_urls.txt)
    # echo "**** Стартуем s3 amazon на дату $ONAPP_NOW ****" >> $ONAPP_REMOVE_PATH/$ONAPP_REMOVE_FILE
    #Быстрая установка
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~" `date` ": Старт удаления $ONAPP_PROVIDER_INPUT-$ONAPP_DAYS_INPUT дней, файлы ранее чем '$ONAPP_REMOVE_DATE'~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    #Детальная проверка по каждой
    KEYS_PATH=($(jq -r '.[] | select(.days == '$ONAPP_DAYS') | select(.provider == '$ONAPP_PROVIDER') | .bucket'  $ONAPP_REMOVE_FILE))
    KEYS_LINK=($(jq -r '.[] | select(.days == '$ONAPP_DAYS') | select(.provider == '$ONAPP_PROVIDER') | .prefix'  $ONAPP_REMOVE_FILE))
    KEYS_PROFILE=($(jq -r '.[] | select(.days == '$ONAPP_DAYS') | select(.provider == '$ONAPP_PROVIDER') | .profile'  $ONAPP_REMOVE_FILE))
    KEYS_ENDPOINT=($(jq -r '.[] | select(.days == '$ONAPP_DAYS') | select(.provider == '$ONAPP_PROVIDER') | .endpoint'  $ONAPP_REMOVE_FILE))
    # echo `date` ": Старт копирования" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE
    # echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_PATH_LOG/$ONAPP_BACKUP_LOG_FILE

    for ((i=0;i<${#KEYS_PATH[@]};i++))
    do
        # echo 'aws s3api list-objects-v2 --profile '${KEYS_PROFILE[$i]}' --endpoint-url='${KEYS_ENDPOINT[$i]}'  --bucket "'${KEYS_PATH[$i]}'" --prefix '${KEYS_LINK[$i]}' --query "Contents[?LastModified<='"'$ONAPP_REMOVE_DATE'"'].{key: Key}" --output text'
        # aws s3api list-objects-v2 --profile "" --endpoint-url ""  --bucket "" --prefix "" --query "Contents[?LastModified<='"'$ONAPP_REMOVE_DATE'"'].{key: Key}" --output text | xargs -n1 -t -I 'KEY' aws s3 rm s3://'${KEYS_PATH[$i]}'/'"'KEY'"'
        # echo 'aws s3api list-objects-v2 --profile "${KEYS_PROFILE[$i]}" --endpoint-url "${KEYS_ENDPOINT[$i]}"  --bucket "${KEYS_PATH[$i]}" --prefix "${KEYS_LINK[$i]}" --query "Contents[?LastModified<='$ONAPP_REMOVE_DATE'].{key: Key}" --output text | xargs -n1 -t -I 'KEY' aws s3 rm s3://${KEYS_PATH[$i]}/'KEY''
        aws s3api list-objects-v2 --profile "${KEYS_PROFILE[$i]}" --endpoint-url ${KEYS_ENDPOINT[$i]}  --bucket "${KEYS_PATH[$i]}" --prefix "${KEYS_LINK[$i]}" --query "Contents[?LastModified<='$ONAPP_REMOVE_DATE'].{key: Key}" --output text | xargs -n1 -t -I 'KEY' aws s3 rm --profile "${KEYS_PROFILE[$i]}" --endpoint-url ${KEYS_ENDPOINT[$i]} s3://${KEYS_PATH[$i]}/'KEY'
        export ONP_TASK_NAME="Провайдер $ONAPP_PROVIDER_INPUT-$ONAPP_DAYS_INPUT дней, файлы ранее чем '$ONAPP_REMOVE_DATE'"
    done
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~" `date` ": Завершение удаления $ONAPP_PROVIDER_INPUT-$ONAPP_DAYS_INPUT дней, файлы ранее чем '$ONAPP_REMOVE_DATE'~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}
export -f onp_remove_s3_func


onp_remove_s3 () {
    ONP_REMOVE_FUNC=onp_remove_s3_func
    ONAPP_PROVIDER_INPUT_1=amazon
    ONAPP_DAYS_INPUT_1_1=35
    ONAPP_DAYS_INPUT_1_2=200
    ONAPP_PROVIDER_INPUT_2=selectel
    ONAPP_DAYS_INPUT_2_1=35
    ONAPP_DAYS_INPUT_2_2=100
    ONAPP_PROVIDER_INPUT_3=yandex
    ONAPP_DAYS_INPUT_3_1=35
    ONAPP_DAYS_INPUT_3_2=100

    ONAPP_BACKUP_ALL_PATH_LOG=/mnt/backup_urls/remove
    ONAPP_NOW=$(date +%b_%d_%y____%H-%M__%A)
    ONAPP_BACKUP_ALL_LOG_FILE=log_remove_s3_temp.log
    ONAPP_BACKUP_ALL_LOG_FILE_FINAL=log_remove_s3.log
    echo "############################# $ONAPP_NOW ########################################" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    # Часть команды
    onprsync_s3_inline_func () {
        # echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
        # echo -n `date` ": Старт копирования" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
        START_ALL_INC=$(date +%s)
            onp_remove_s3_func
        END_ALL_INC=$(date +%s)
        DIFF_ALL_INC=$(( $END_ALL_INC - $START_ALL_INC ))
        DIFFTIME_ALL_INC=$(displaytime $DIFF_ALL_INC)
        echo -e "   $DIFFTIME_ALL_INC   $ONP_TASK_NAME" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    }
    export -f onprsync_s3_inline_func

    START_ALL=$(date +%s)
    # amazon s3
    export ONAPP_PROVIDER_INPUT=$ONAPP_PROVIDER_INPUT_1
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_1_1
        onprsync_s3_inline_func
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_1_2
        onprsync_s3_inline_func
    # selectel s3
    export ONAPP_PROVIDER_INPUT=$ONAPP_PROVIDER_INPUT_2
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_2_1
        onprsync_s3_inline_func
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_2_2
        onprsync_s3_inline_func
    # yandex s3
    export ONAPP_PROVIDER_INPUT=$ONAPP_PROVIDER_INPUT_3
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_3_1
        onprsync_s3_inline_func
    export ONAPP_DAYS_INPUT=$ONAPP_DAYS_INPUT_3_2
        onprsync_s3_inline_func

    END_ALL=$(date +%s)
    DIFF_ALL=$(( $END_ALL - $START_ALL ))
    DIFFTIME_ALL=$(displaytime $DIFF_ALL)
    echo -e "[ $DIFFTIME_ALL ] общее время удаления" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    echo "" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    echo "" >> $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
    cat $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE | cat - $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE_FINAL | sponge $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE_FINAL
    rm $ONAPP_BACKUP_ALL_PATH_LOG/$ONAPP_BACKUP_ALL_LOG_FILE
}
export -f onp_remove_s3


command_autoupdate_always_func(){

    ONP_UPDATE_MODE_CHECK=$ONP_UPDATE_MODE



    if [ $ONP_UPDATE_MODE_CHECK = 1 ]; then
        ONP_RUN=On;
        ONP_RM=Off;
    else
        ONP_RUN=Off;
        ONP_RM=On;
    fi;

    # Функции
    autoupdate_run_func(){
    if [ $ONP_RUN = "On" ]; then
        if [ ! -f $HOME/onp/autoupdate_command_on ]; then
            touch $HOME/onp/autoupdate_command_on;
            if [ -f $HOME/onp/autoupdate_command_on_allow ]; then
                autoupdate_command_list
                # rm $HOME/onp/autoupdate_command_on_allow;
                touch $HOME/onp/autoupdate_command_on_deny;
            fi;
        fi;
    fi;
    }
    export -f autoupdate_run_func


    autoupdate_rm_func(){
    if [ $ONP_RM = "On" ]; then
        if [ -f $HOME/onp/autoupdate_command_on ]; then
            rm -f $HOME/onp/autoupdate_command_on;
            touch $HOME/onp/autoupdate_command_on_allow;
            rm -f $HOME/onp/autoupdate_command_on_deny;
        fi;
    fi;
    }
    export -f autoupdate_rm_func


    # if [ ! -f $HOME/onp/autoupdate_command_on_deny ]; then
    #     autoupdate_run_func
    # fi;

    autoupdate_run_func
    autoupdate_rm_func

}
export -f command_autoupdate_always_func




onp_send_log(){
    ONP_VAR=$(find ~ -name "test_process_*.log")
    echo "В приложении логи с сервера $HOSTNAME" | mail -s "Логи сервера $HOSTNAME" nikolaev@cmilestone.ru --attach $ONP_VAR
    unset ONP_VAR
}
export -f onp_send_log