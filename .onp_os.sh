#!/bin/bash
# onp_os
#
#
export onp_os_title="Файл с выбором ОС......[OK]"


onp_os_debian_locale() {
    # Install locales package
    sudo apt-get install -y locales

    # Uncomment en_US.UTF-8 for inclusion in generation
    sudo sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen
    sudo sed -i 's/^# *\(ru_RU.UTF-8\)/\1/' /etc/locale.gen

    # Generate locale
    sudo locale-gen --purge en_US.UTF-8 ru_RU.UTF-8


    # Export env vars
    echo "export LC_ALL=ru_RU.UTF-8" >> $HOME/.bashrc
    echo "export LANG=ru_RU.UTF-8" >> $HOME/.bashrc
    echo "export LANGUAGE=ru_RU.UTF-8" >> $HOME/.bashrc
    source $HOME/.profile
    echo "*******************************"
    echo "*    ЯЗЫКИ DEBIAN ПОСТАВЛЕНЫ  *"
    echo "*******************************"
}
export -f onp_os_debian_locale



locale_ru_ubuntu() {
    # Install locales package
    sudo update-locale LANG=ru_RU.UTF-8
    source $HOME/.profile
    date
    echo "**********************************"
    echo "* ПОСТАВЛЕН РУССКИЙ  ЯЗЫК UBUNTU *"
    echo "**********************************"
}
export -f locale_ru_ubuntu

locale_en_ubuntu() {
    # Install locales package
    sudo update-locale LANG=en_US.UTF-8
    source $HOME/.profile
    date
    echo "**********************************"
    echo "* ПОСТАВЛЕН ENGLISH  ЯЗЫК UBUNTU *"
    echo "**********************************"
}
export -f locale_en_ubuntu



onp_os_ubuntu_locale() {
    # Install locales package
    sudo apt install apt-utils
    sudo apt install language-pack-ru -y
    setupcon -f
    sudo update-locale LANG=ru_RU.UTF-8
    sudo locale-gen --purge en_US.UTF-8 ru_RU.UTF-8
    source $HOME/.profile
    date

    echo "*******************************"
    echo "*   ЯЗЫКИ UBUNTU ПОСТАВЛЕНЫ   *"
    echo "*******************************"
}
export -f onp_os_ubuntu_locale



# --- End Definitions Section ---
# check if we are being sourced by another script or shell
# [[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }



onp_install_debian_minimal() {

    #Проверка всех загрузок
    source $HOME/.onp_alias
    # source $HOME/onp/.onp_soft.sh
    # source $HOME/onp/.onp_os.sh
            ___onp_config_setup_line

    #Лог времени
    echo "Обновляем систему onp_install_debian_minimal"
    onpdf

    #Ставим общий сетап
    echo "Ставим общий сетап"
    onp_config_cli_color
            ___onp_config_setup_line

    #Обновляем
    onp_soft_full_remove_and_all_update
            ___onp_config_setup_line

    ####################################################################################################################
    #Ставим локаль Debian
    onp_os_debian_locale
            ___onp_config_setup_line
    ####################################################################################################################


    #Диск подкачки
    onp_config_disk_swap
            ___onp_config_setup_line

    #Ставим время
    onp_config_time
            ___onp_config_setup_line

    #Общие программы
    onpapt_list_minimal
            ___onp_config_setup_line


    #Простое обновление
    onp_soft_simple_update
            ___onp_config_setup_line

    # Лог времени
            ______onp_config_log_line


    #Консоль цвета
    onpnano

    #Закладки
    onpbashmarks

    #окончание
    onp_config_end_update
            ___onp_config_setup_line
}
export -f onp_install_debian_minimal