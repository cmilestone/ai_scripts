#!/bin/bash
# onp_alias
#
#
export onp_app_title="Файл с app......[OK]"

onp_addonp_crontab() {
    # crontab -l
    (crontab -l && echo "#") | crontab && crontab -l
    (crontab -l && echo "############## ONP crontab ##############" && echo 'MAILTO=""' && echo "SHELL=/bin/bash" && echo "*/5 * * * * . $HOME/.profile && onpdrepo >/dev/null 2>&1" && echo "############ // ONP crontab  ############") | crontab && crontab -l
}
export -f onp_addonp_crontab




onp_add_local_onp_crontab() {
    (crontab -l && echo "#") | crontab && crontab -l
    (crontab -l && echo "############## ONP crontab ##############" && echo "SHELL=/bin/bash" && echo "*/5 * * * * . $HOME/.profile && onpdrepo >/dev/null 2>&1" && echo "############ // ONP crontab  ############") | crontab && crontab -l
}
export -f onp_add_local_onp_crontab


onp_add_onp_bash () {
    #######################################################################
    # echo 'echo ""'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "if [ -d ~/.bash_completion.d ]; then"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "  for file in ~/.bash_completion.d/*; do"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo '      . $file'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "  done"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "fi"  | sudo tee -a $HOME/.bashrc > /dev/null
    #Редактор для nnn
    echo "#####Редактор для nnn"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "export EDITOR=nano"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "export VISUAL=nano"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "############################################################"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo ""'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'ai_path=$(pwd)'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'ONPSYS_VERSION_FULL="$(lsb_release -a 2>&1 | grep  'Description' | cut -f2)"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'ONPSYS_VERSION_ID="$(lsb_release -a 2>&1 | grep  'Distributor' | cut -f2)"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'ONPSYS_VERSION_RELEASE="$(lsb_release -a 2>&1 | grep  'Release' | cut -f2)"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'ONPSYS_VERSION_CODENAME="$(lsb_release -a 2>&1 | grep  'Codename' | cut -f2)"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "          "$ONPSYS_VERSION_FULL"         "' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "***********************************************"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "*       Добро пожаловать в $ONPSYS_VERSION_ID $ONPSYS_VERSION_RELEASE       *"' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "*     --------  загружен профиль:  -------    *"' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "*     ------------ $USER BASH  -----------     *"' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "*              СПРАВКА:   onph                *"' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "***********************************************"' | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "Каталог: $ai_path"'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo 'echo "Версия ONP $ONAPP_VER"'  | sudo tee -a $HOME/.bashrc > /dev/null
}
export -f onp_add_onp_bash


onp_add_onp_bash_ver () {
    echo 'echo "Версия ONP $ONAPP_VER"'  | sudo tee -a $HOME/.bashrc > /dev/null
    }
export -f onp_add_onp_bash_ver