#!/bin/bash
# onp_alias
#
#


onpfind(){
    echo "~~~~ dpkg --get-selections | grep php* ~~~~~"
    dpkg --get-selections | grep $1
    echo "~~~~~~~~~~  Поиск закончен ~~~~~~~~~~"
}
export -f onpfind


onpu() {
    echo "~~~~~~~~ Начинаю простое обновление пакетов ~~~~~~~~"
    sudo apt-get update -y
    echo "~~~~~~~~ Простое обновление пакетов завершено ~~~~~~~~"
}
export -f onpu

onpuu() {
    echo "~~~~~~~~ Начинаю полное обновление пакетов и репозитория ~~~~~~~~"
    sudo apt-get update -y
    sudo apt-get upgrade -y
    sudo apt-get update -y
    echo "~~~~~~~~ Полное обновление пакетов и репозитория  завершено ~~~~~~~~"
}
export -f onpuu


onpuupgrade() {
    echo "~~~~~~~~ Начинаю обновление ядра  ~~~~~~~~"
    # sudo apt-get update -y
    sudo apt-get dist-upgrade -y
    sudo apt full-upgrade -y
    echo "~~~~~~~~ Полное обновление обновление ядра  завершено ~~~~~~~~"
}
export -f onpuupgrade


onpur(){
    echo "~~~~~~~~ Начинаю удаление ненужных пакетов ~~~~~~~~"
    sudo apt autoremove -y
    sudo autoremove --purge -y
    sudo apt update -y
    echo "~~~~~~~~ Удаление ненужных пакетов завершено ~~~~~~~~"
}
export -f onpur


onpufix(){
    echo "~~~~~~~~ Начинаю полную очистку программ и репозитория ~~~~~~~~"
    sudo apt update -y
    sudo apt autoremove -y
    sudo apt autoremove --purge -y
    sudo apt --fix-broken install -y
    sudo apt-get dist-upgrade -y
    sudo apt full-upgrade -y
    echo "~~~~~~~~ Полная очистка программ и репозитория завершена ~~~~~~~~"
}
export -f onpufix


onpufixhard(){
    echo "~~~~~~~~ Начинаю onpufixhard программ и репозитория ~~~~~~~~"
    sudo apt update -y
    sudo apt-get clean
    sudo apt-get autoclean
    sudo apt-get autoremove
    sudo apt autoremove
    sudo apt autoremove --purge
    sudo dpkg --configure -a
    apt list --upgradable
    sudo aptitude safe-upgrade
    echo "~~~~~~~~ onpufixhard программ и репозитория завершена ~~~~~~~~"
}
export -f onpufixhard




onpvirtmin(){
    echo "~~~~~~~~~~~~ КОНФИГУРАЦИЯ WEBMIN ~~~~~~~~~~~~~~~~~~~~"
    virtualmin check-config
    echo "~~~~~~~~~~~~ ХОСТЫ WEBMIN ~~~~~~~~~~~~~~~~~~~~"
    virtualmin validate-domains --all-domains --feature web
}
export -f onpvirtmin



rhelp(){
    echo "ПЕРЕЗАПУСК служб:   |     rall    |    rsrv    |    apache:rap     |    nginx: rngx     |      mysql: rdb       |       php73-fpm: rfpm      |     omemcached: rmem "
}
export -f rhelp


rall () {
    ONP_RESTART_VAR_1="apache2"
    ONP_RESTART_VAR_2="mysql"
    ONP_RESTART_VAR_3="php8*"
    ONP_RESTART_VAR_4="php7*"
    ONP_RESTART_VAR_5="php5*"
    ONP_RESTART_VAR_6="memcached"
    ONP_RESTART_VAR_7="redis-server"
    ONP_RESTART_VAR_8="varnish"
    ONP_RESTART_VAR_9="haproxy"
    ONP_RESTART_VAR_10="nginx"
    ONP_RESTART_VAR_11="postfix"

    # Часть команды
    ONP_RESTART_FUNC_LOCAL='sudo systemctl -q is-active'

    rall_inline_func () {
        # echo $ONP_RESTART_TEMPVAR
        ONP_VAR_RALL=$(ps -A | grep $ONP_RESTART_TEMPVAR)
        if [ ! -z "$ONP_VAR_RALL" ]; then
            # echo 111111
            echo -n "Перезагрузка $ONP_RESTART_TEMPVAR ......"
            $ONP_RESTART_FUNC_LOCAL $ONP_RESTART_TEMPVAR  && sudo systemctl restart $ONP_RESTART_TEMPVAR && echo -n "[OK]" && sudo systemctl status $ONP_RESTART_TEMPVAR  | grep running || echo -e "$ONP_RESTART_TEMPVAR не запущен"
            # unset ONP_RESTART_TEMPVAR
        # else
            # echo 2222222
        #     echo "$LIST_ROLL" found;
        fi
    }
    export -f rall_inline_func

    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_1
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_2
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_3
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_4
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_5
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_6
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_7
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_8
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_9
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_10
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_10
        rall_inline_func
    echo -e "******************************"
    echo -e "Сервисы  перезапущены"
    echo -e "Перезапуск службы rest *name, журнал rjurn *name  //  journalctl -xe | grep failed"
    echo -e "******************************"
    sudo journalctl -xe | grep failed
}
export -f rall


rwww() {
    ONP_RESTART_VAR_1="apache2"
    ONP_RESTART_VAR_2="nginx"
    ONP_RESTART_VAR_3="php*"
    ONP_RESTART_VAR_4="haproxy"
    ONP_RESTART_VAR_5="varnish"
    ONP_RESTART_VAR_6="memcached"
    ONP_RESTART_VAR_8="redis-server"
    ONP_RESTART_VAR_9="mysql"

    # Часть команды
    ONP_RESTART_FUNC_LOCAL='sudo systemctl -q is-active'
    rall_inline_func () {
        # echo $ONP_RESTART_TEMPVAR
        ONP_VAR_RALL=$(ps -A | grep $ONP_RESTART_TEMPVAR)
        if [ ! -z "$ONP_VAR_RALL" ]; then
            # echo 111111
            echo -n "Перезагрузка $ONP_RESTART_TEMPVAR ......"
            $ONP_RESTART_FUNC_LOCAL $ONP_RESTART_TEMPVAR  && sudo systemctl restart $ONP_RESTART_TEMPVAR && echo -n "[OK]" && sudo systemctl status $ONP_RESTART_TEMPVAR  | grep running || echo -e "$ONP_RESTART_TEMPVAR не запущен"
            # unset ONP_RESTART_TEMPVAR
        # else
            # echo 2222222
        #     echo "$LIST_ROLL" found;
        fi
    }
    export -f rall_inline_func

    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_1
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_2
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_3
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_4
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_5
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_6
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_7
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_8
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_9
        rall_inline_func
    echo -e "Перезапуск службы rest *name, журнал rjurn *name  //  journalctl -xe | grep failed"
    echo -e "******************************"
    echo -e "Сервисы  перезапущены"
    echo -e "Перезапуск одной службы rest *name, журнал rjurn *name "
    echo -e "******************************"
    sudo journalctl -xe | grep failed

}
export -f rwww

rsrv() {
    ONP_RESTART_VAR_1="fail2ban"
    ONP_RESTART_VAR_2="firewalld"

    # Часть команды
    ONP_RESTART_FUNC_LOCAL='sudo systemctl -q is-active'
    rall_inline_func () {
        # echo $ONP_RESTART_TEMPVAR
        ONP_VAR_RALL=$(ps -A | grep $ONP_RESTART_TEMPVAR)
        if [ ! -z "$ONP_VAR_RALL" ]; then
            # echo 111111
            echo -n "Перезагрузка $ONP_RESTART_TEMPVAR ......"
            $ONP_RESTART_FUNC_LOCAL $ONP_RESTART_TEMPVAR  && sudo systemctl restart $ONP_RESTART_TEMPVAR && echo -n "[OK]" && sudo systemctl status $ONP_RESTART_TEMPVAR  | grep running || echo -e "$ONP_RESTART_TEMPVAR не запущен"
            # unset ONP_RESTART_TEMPVAR
        else
            echo -n "Перезагрузка $ONP_RESTART_TEMPVAR ......"
            $ONP_RESTART_FUNC_LOCAL $ONP_RESTART_TEMPVAR  && sudo systemctl restart $ONP_RESTART_TEMPVAR && echo -n "[OK]" && sudo systemctl status $ONP_RESTART_TEMPVAR  | grep running || echo -e "$ONP_RESTART_TEMPVAR не запущен"
            # echo 2222222
        #     echo "$LIST_ROLL" found;
        fi
    }
    export -f rall_inline_func


    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_1
        rall_inline_func
    ONP_RESTART_TEMPVAR=$ONP_RESTART_VAR_2
        rall_inline_func
    echo -e "******************************"
    echo -e "Сервисы  перезапущены"
    echo -e "Перезапуск службы rest *name, журнал rjurn *name  //  journalctl -xe | grep failed"
    echo -e " Лог onptail "
    echo -e "******************************"
    sudo journalctl -xe | grep failed
}
export -f rsrv

ffrun() {
    ONP_RESTART_VAR_1="fail2ban"
    ONP_RESTART_VAR_2="firewalld"
    service $ONP_RESTART_VAR_1 status
    echo -e "******************************"
    echo -e " "
    echo -e " "
    echo -e "Перезапуск службы $ONP_RESTART_VAR_1>>>>> rest $ONP_RESTART_VAR_1, журнал>>>>> rjurn $ONP_RESTART_VAR_1  //  journalctl -xe | grep $ONP_RESTART_VAR_1 "
    echo -e " Лог onptail "
    echo -e " "
    echo -e " "
    echo -e "******************************"

    service $ONP_RESTART_VAR_2 status
    echo -e "******************************"
    echo -e " "
    echo -e " "
    echo -e "Перезапуск службы $ONP_RESTART_VAR_2>>>>> rest $ONP_RESTART_VAR_2, журнал>>>>> rjurn $ONP_RESTART_VAR_2  //  journalctl -xe | grep $ONP_RESTART_VAR_2 "
    echo -e " Лог onptail "
    echo -e " "
    echo -e " "
    echo -e "******************************"
    echo -e "Открытые порты службы firewalld: "
    echo -e " "
    echo -e " "
    firewall-cmd --list-ports
    echo -e " "
    echo -e " "
    echo -e "ПРОВЕРИТЬ jail: onptail "

}
export -f ffrun


alias rsrvrun=ffrun



onp_restart_func () {
    echo ""
    echo ""
    # echo $ONP_RESTART_VAR
    # echo -e "******************************"
    echo -n "Перезагрузка $ONP_RESTART_VAR_TITLE ......"
    sudo systemctl restart $ONP_RESTART_VAR_TITLE
    echo -e "[OK]"
    echo ""
    echo ""
    systemctl status $ONP_RESTART_VAR
    echo ""
    echo ""
    # echo -e "******************************"
    echo -e "Сервис [$ONP_RESTART_VAR_TITLE] перезапущен"
    unset ONP_RESTART_VAR && unset ONP_RESTART_VAR_TITLE
    echo -e "Перезапуск службы rest *name, журнал rjurn *name, start *name, журнал stop *name  "
    sudo journalctl -xe | grep failed

}
export -f onp_restart_func

onp_start_func () {
    echo ""
    echo ""
    # echo $ONP_RESTART_VAR
    # echo -e "******************************"
    echo -n "Запуск $ONP_START_VAR_TITLE ......"
    sudo systemctl start $ONP_START_VAR_TITLE
    echo -e "[OK]"
    echo ""
    echo ""
    systemctl status $ONP_START_VAR
    echo ""
    echo ""
    # echo -e "******************************"
    echo -e "Сервис [$ONP_START_VAR_TITLE] запущен"
    unset ONP_START_VAR && unset ONP_START_VAR_TITLE
    echo -e "Перезапуск службы rest *name, журнал rjurn *name, start *name, журнал stop *name  "
    sudo journalctl -xe | grep failed
}
export -f onp_start_func


onp_stop_func () {
    echo ""
    echo ""
    # echo $ONP_RESTART_VAR
    # echo -e "******************************"
    echo -n "Запуск $ONP_STOP_VAR_TITLE ......"
    sudo systemctl stop $ONP_STOP_VAR_TITLE
    echo -e "[OK]"
    echo ""
    echo ""
    systemctl status $ONP_STOP_VAR
    echo ""
    echo ""
    # echo -e "******************************"
    echo -e "Сервис [$ONP_STOP_VAR_TITLE] остановлен"
    unset ONP_STOP_VAR && unset ONP_STOP_VAR_TITLE
    echo -e "Перезапуск службы rest *name, журнал rjurn *name, start *name, журнал stop *name  "
    sudo journalctl -xe | grep failed
}
export -f onp_stop_func


rest () {
    ONP_RESTART_VAR="$@"
    echo "Перезапуск службы $@"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
        onp_restart_func
}
export -f rest

start () {
    ONP_START_VAR="$@"
    echo "Запуск службы $@"
    ONP_START_VAR_TITLE=$ONP_START_VAR
        onp_start_func
}
export -f start

stop () {
    ONP_STOP_VAR="$@"
    echo "Остановка службы $@"
    ONP_STOP_VAR_TITLE=$ONP_STOP_VAR
        onp_stop_func
}
export -f stop

rap(){
    ONP_RESTART_VAR="apache2"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rap


rngx () {
    ONP_RESTART_VAR="nginx"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rngx

rdb () {
    ONP_RESTART_VAR="mysql"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rdb


rfpm () {
    echo "Перезапуск всех версий 7*"
    ONP_RESTART_VAR="php7*"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rfpm


rfpm5 () {
    echo "Перезапуск всех версий 5*"
    ONP_RESTART_VAR="php5*"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rfpm5


rfpmv () {
    echo "Введите версию вида 7.0 7.4 8.1 8.2 8.3 8.4"
    read FPMVER
    ONP_RESTART_VAR="php$FPMVER-fpm"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rfpmv

rmem () {
    ONP_RESTART_VAR="memcached"
    ONP_RESTART_VAR_TITLE=$ONP_RESTART_VAR
    onp_restart_func
}
export -f rmem


jhelp(){
    echo ""
    echo ""
    echo " nginx: jngx     |      mysql: jdb       |       php73-fpm: jfpm      |     omemcached: jmem "
    echo ""
    echo ""
}
export -f jhelp

onp_journal_func () {
    systemctl status network $ONP_JOURNAL_VAR
    echo "Журнал для $ONP_JOURNAL_VAR [OK]"
    journalctl -u $ONP_JOURNAL_VAR.service
    unset ONP_JOURNAL_VAR
    echo ""
    echo ""
}
export -f onp_journal_func

jngx () {
    ONP_JOURNAL_VAR="nginx"
    onp_journal_func
}
export -f jngx

jdb () {
    ONP_JOURNAL_VAR="mysql"
    onp_journal_func
}
export -f jdb

jfpm () {
    ONP_JOURNAL_VAR="php7.3-fpm"
    onp_journal_func
}
export -f jfpm

jmem () {
    ONP_JOURNAL_VAR="memcached"
    onp_journal_func
}
export -f jmem



rjurn () {
    ONP_JOURNAL_VAR="$@"
    echo "Журнал службы $@"
        onp_journal_func
    echo -e "Перезапуск службы rest *name, журнал rjurn *name "
}
export -f rjurn

aibashfull(){
    source $HOME/.bashrc
}
export -f aibashfull

onpln() {
    echo "onpln символьная ссылка symbol link"
    echo "ln -l  рассматривать ИМЯ_ССЫЛКИ, как обычный файл, если это символьная ссылка на каталог"

    echo "ln -s   создавать символьные ссылки, вместо жёстких ссылок"
    echo "ln -s /mnt/out_folder_откуда_брать_папку/ /mnt/in_folder_куда вставить/"
    echo "ln -s /mnt/iso    /home/other_place/public_html/iso"
    echo "       |это папка  |откуда берем"

    echo "ln -s /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/conf.d/$PHP_FILE_"
    echo "ls -l /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/conf.d/$PHP_FILE_"
}
export -f onpln



onpinifpm(){
    ls /etc/php/
    echo "php.ini Версии Укажите  7.4 8.1 8.2 8.3 8.4(рано) "
    read ONP_PHP_VER_FPM
    PHP_FILE_=00-onapp-custom.ini
    # rm /etc/php/$PHP_FILE_
    cp $HOME/onp/$PHP_FILE_ /etc/php/$PHP_FILE_
    # ln -l  рассматривать ИМЯ_ССЫЛКИ, как обычный файл, если это символьная ссылка на каталог

    # ln -s   создавать символьные ссылки, вместо жёстких ссылок
    # ln -s /mnt/out_folder_откуда_брать_папку/ /mnt/in_folder_куда вставить/
    # ln -s /mnt/iso    /home/other_place/public_html/iso
    #       |это папка  |откуда берем

    ln -s /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/conf.d/$PHP_FILE_
    ls -l /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/conf.d/$PHP_FILE_
    echo "Править конфиг можно по команде"
    echo "nano /etc/php/$PHP_FILE_"
    rall
}
export -f onpinifpm


onpinifpmconf(){
    ls /etc/php/
    echo "php.ini Версии Укажите 7.4 8.1 8.2 8.3 8.4(рано)"
    read ONP_PHP_VER_FPM
    PHP_FILE_=10_fpm_onp_pool_d_default.conf
    # rm /etc/php/$PHP_FILE_
    cp $HOME/onp/$PHP_FILE_ /etc/php/$PHP_FILE_
    ln -s /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/pool.d/$PHP_FILE_
    ls -l /etc/php/$PHP_FILE_ /etc/php/$ONP_PHP_VER_FPM/fpm/pool.d/$PHP_FILE_
    echo "Править конфиг можно по команде"
    echo "nano /etc/php/$PHP_FILE_"
    rall
}
export -f onpinifpmconf


onpeditphpini() {
    ls /etc/php/*
    nano /etc/php/00-onapp-custom.ini
}
export -f onpeditphpini



onpbacula(){
    echo "Приготовьте пароль для новой MYSQL базы Bacula"
    onpapt bacula-director-mysql
    onpapt bacula-server bacula-client
    echo "Bacula установлена"
}
export -f onpbacula

onprun(){
    netstat -plntu | grep 'nginx\|apache*\|varnish*\|redi*\|haproxy\|php-fpm\|mysql\|proftpd\|ssh*\|memcached\|docker*\|php*'
}
export -f onprun

onprunall(){
    netstat -plntu
}
export -f onprunall

onprunports(){
    netstat -plntu | grep ':80\|:443\|:21\|:8888\|nginx\|apache2\|varnish'
}
export -f onprunports

alias onprunp=onprunports


onpreinstall(){
    onpupdate_files_on_demand_func
}
export -f onpreinstall


onpnano(){
    bash -c "curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sh"
    onpupdate_files_on_demand_func
}
export -f onpnano

onpsoft_ubuntu(){
    # sudo apt install apt-utils
    sudo add-apt-repository ppa:ultradvorka/ppa && sudo apt-get update && sudo apt-get install hstr && hstr --show-configuration >> ~/.bashrc && . ~/.bashrc
    onpapt fd-find nnn ranger
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install
    mkdir ~/.bash_completion.d && curl "https://raw.githubusercontent.com/rupa/z/master/{z.sh}" \
    -o ~/.bash_completion.d/"#1" && curl "https://raw.githubusercontent.com/changyuheng/fz/master/{fz.sh}" \
    -o ~/.bash_completion.d/z"#1"
    onpapt fzf silversearcher-ag ack
    # pip3 install --user advance-touch
    # cp /etc/netplan/01-netcfg.yaml /root/netplan_original.yaml.onp
    # echo "/etc/netplan/01-netcfg.yaml"
}
export -f onpsoft_ubuntu

onpnetplanrun(){
    cp /etc/netplan/01-netcfg.yaml /root/netplan_copy__`date +%Y_%m_%d_%H_%M`.yaml.onp
    cp /root/netplan_original.yaml.onp /etc/netplan/01-netcfg.yaml
    netplan try
    netplan apply
    echo "~~~~~~~~~~~~~~~~~~~~~~ ПРАВКА netplan~~~~~~~~~~~~~~~~~~~~~~"
    echo "/etc/netplan/01-netcfg.yaml"
}
export -f onpnetplanrun

onpnetplanbackup(){
    cp /etc/netplan/01-netcfg.yaml /root/netplan_copy__`date +%Y_%m_%d_%H_%M`.yaml.onp
    cp /etc/netplan/01-netcfg.yaml /root/netplan_original.yaml.onp
    ls /root | grep yaml.onp
    echo "~~~~~~~~~~~~~~~~~~~~~~ ПРАВКА netplan~~~~~~~~~~~~~~~~~~~~~~"
    echo "/etc/netplan/01-netcfg.yaml"
}
export -f onpnetplanbackup

onpbashmarks(){
    cd $HOME
    git clone https://github.com/huyng/bashmarks.git
    cd $HOME/bashmarks
    make install
    echo "source ~/.local/bin/bashmarks.sh" | tee -a $HOME/.bashrc > /dev/null
    source $HOME/.bashrc
}
export -f onpbashmarks


onpfzzz(){
    onpapt fzf
    mkdir ~/.bash_completion.d
    curl "https://raw.githubusercontent.com/rupa/z/master/{z.sh}" \
        -o ~/.bash_completion.d/"#1"
    curl "https://raw.githubusercontent.com/changyuheng/fz/master/{fz.sh}" \
        -o ~/.bash_completion.d/z"#1"
    # echo 'echo ""'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "if [ -d ~/.bash_completion.d ]; then"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "  for file in ~/.bash_completion.d/*; do"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo '      . $file'  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "  done"  | sudo tee -a $HOME/.bashrc > /dev/null
    echo "fi"  | sudo tee -a $HOME/.bashrc > /dev/null
}
export -f onpfzzz

onpchownpwd(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Смена пользователя для локальных файлов ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    lsr
    ALNI_ONPCHOWNPWD_2=$(pwd | sed 's,[^/]*$,,')
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPCHOWNPWD_2
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь текущей папки ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    ALNI_ONPCHOWNPWD_1=$(pwd)
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPCHOWNPWD_1
    CHOWN_PWD=$(pwd)
    echo "Введите юзера Linux в папке $CHOWN_PWD"
    read CHOWN_NAME
    sudo chown -R $CHOWN_NAME:$CHOWN_NAME *
    sudo chown -R $CHOWN_NAME:$CHOWN_NAME .*
    echo "Теперь все во власти $CHOWN_NAME:$CHOWN_NAME в папке $CHOWN_PWD"
}
export -f onpchownpwd

onpchownchmodpwd(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Разрешения файлов и папок для локальных файлов  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь вышестоящей папки ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    ALNI_ONPCHOWNCHMODPWD_2=$(pwd | sed 's,[^/]*$,,')
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPCHOWNCHMODPWD_2
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь текущей папки ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    ALNI_ONPCHOWNCHMODPWD_1=$(pwd)
    stat -c ' %U:%G [%u]    %n'  $ALNI_ONPCHOWNCHMODPWD_1
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Пользователь текущего продакшена внутри ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    stat -c ' %U:%G [%u]    %n'  *
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Введите нужного user для модификации ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    read ONPCHOWNCHMOD_OWNER_VAR
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~ Введите путь, возможно это $ALNI_ONPCHOWNCHMODPWD_1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    read ONPCHOWNCHMOD_PWD_VAR
    ONPCHOWNCHMOD_OWNER=$ONPCHOWNCHMOD_OWNER_VAR # <-- files owner
    ONPCHOWNCHMOD_GROUP=$ONPCHOWNCHMOD_OWNER_VAR # <-- group
    ONPCHOWNCHMOD_ROOT=$ONPCHOWNCHMOD_PWD_VAR # <-- files directory

    # reset to safe defaults
    echo "~~~~~~~~~~~~~~~~ 1/8 ~~~~~~~~~~~~~~~~"
    find ${ONPCHOWNCHMOD_ROOT} -exec chown -v ${ONPCHOWNCHMOD_OWNER}:${ONPCHOWNCHMOD_GROUP} {} \;
    echo "~~~~~~~~~~~~~~~~ 2/8 ~~~~~~~~~~~~~~~~"
    find ${ONPCHOWNCHMOD_ROOT} -type d -exec chmod 755 -v {} \;
    echo "~~~~~~~~~~~~~~~~ 3/8 ~~~~~~~~~~~~~~~~"
    find ${ONPCHOWNCHMOD_ROOT} -type f -exec chmod 644  -v {} \;
    echo "~~~~~~~~~~~~~~~~ КОНЕЦ, ВСЕ ШТАТНО НЕ ССЫТЕ ~~~~~~~~~~~~~~~~"
    unset ALNI_ONPCHOWNCHMODPWD_2
    unset ALNI_ONPCHOWNCHMODPWD_1
    unset ONPCHOWNCHMOD_OWNER_VAR
    unset ONPCHOWNCHMOD_GROUP
    unset ONPCHOWNCHMOD_ROOT
}
export -f onpchownchmodpwd


onplocsshchmod(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Смена прав для файлов ssh  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    chmod 700 ~/.ssh
    chmod 644 ~/.ssh/authorized_keys
    chmod 644 ~/.ssh/known_hosts
    chmod 644 ~/.ssh/config
    chmod 600 ~/.ssh/id_rsa
    chmod 644 ~/.ssh/id_rsa.pub
}
export -f onplocsshchmod


onpupdatenodenpm(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Версии npm и node   ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    sudo npm -v && sudo node -v
    onpuuffnoreboot
    sudo curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash -
    sudo apt-get install -y nodejs
    sudo npm install -g npm@latest
    sudo npm install -g -y update
    onpuuffnoreboot
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Версии npm и node  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    npm -v && node -v
}
export -f onpupdatenodenpm


onpcert(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Работа с сертификатами  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    sudo certbot certificates | grep 'Certificate Name'
    echo "sudo certbot certificates    |    sudo certbot --nginx -d domain"
}
export -f onpcert



onpclam(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Обновляем CLAMAV  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    service clamav-freshclam stop
    sudo freshclam
    service clamav-freshclam start
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  Обновляем CLAMAV  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}
export -f onpclam

onpallphp(){
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  ###### Установить мульти версии PHP  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    onp_multiphp_ubuntu
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  ###### Установить все для нужной версии PHP  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    onpiphp
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  ###### Установить все для нужной версии PHP-FPM  ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    onpinifpmconf
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~  ###### Установить все для нужной версии PHP-INI прокинуть кастом ~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    onpinifpm
}
export -f onpallphp


