#!/bin/bash
#
# onp_config_
#
export ONAPP_VER_HELP="~~~~~~~~~~~ Версия HELP $DATE_4_VER_HELP ~~~~~~~~~~~"



onp-apps-help-list () {
    echo $ONAPP_VER_HELP
    #5
    TITLE_COL=$(tput setaf 5)
    H_COL=$(tput setaf 11)
    M_COL=$(tput setaf 8)
    D_COL=$(tput setaf 8)
    # DEVIDER_LIST=$(echo "    |    ")
    # DEVIDER_LIST_="'%s%s\n' $DEVIDER_LIST"
    echo ""
    echo "$TITLE_COL"


    echo "~~~~~~~ Список системных команд ~~~~~~~"
    declare -a help_arr_1=(
        # "onpconfphp $M_COL  листинг PHP версия для Webmin"
        "rhelp $M_COL  хелп по перезапуску служб"
        "jhelp $M_COL  хелп по перезапуску служб"
        "rjurn ** $M_COL  журнал по перезапуску служб"
        # "command $M_COL command_text_here"
        "rest/start/stop name$M_COL перезапуск службы"

        # "command $M_COL command_text_here"
        )
    for i in "${help_arr_1[@]}"
    do
        echo -n "$H_COL $i"
        echo -n "   $D_COL/   "
    done
    echo -e ""
    echo -e "$TITLE_COL"



    echo "~~~~~~~ Список общих команд ~~~~~~~"
    declare -a help_arr_2=(
        "onpmy $M_COL правка своего конфига на одельно сервере"
        # "onplocchown $M_COL Смена пользователя для локальных файлов"
        # "onplocsshchmod $M_COL Смена прав для файлов ssh "
        # "onpwebwpchmod $M_COL Разрешения файлов и папок для работы WP"
        # "onp_multiphp_mods $M_COL установка модулей PHP"
        # "onp_php_mod_tidy $M_COL command_text_here"
        # "onp_php_mod_brotli $M_COL command_text_here"
        # "onp_multiphp_debian $M_COL Подготовка к мультиверсиям php Debian"
        # "onp_multiphp_ubuntu $M_COL Подготовка к мультиверсиям php Ubuntu"
        # "onpwebsitemysqlbackup $M_COL Копия сайта"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "onpiphp $M_COL установка версии PHP"
        # "onpinifpm $M_COL символьная ссылка PHP.INI"
        # "onpf $M_COL профиль"
        "onptarfull $M_COL сделать копию текущей папки в виде tar в ней"
        "onptotar $M_COL сделать папку для архива"
        "onpdfd $M_COL апдейт"
        "onpsys $M_COL  сведения о системе"
        "onpname $M_COL  название сервера"
        "onpdu $M_COL  апдейт системы ONP"
        # "onplistrepo $M_COL  репо лист"
        "onpuuffreboot $M_COL  полный апдейт + ребут"
        "onpuuffnoreboot $M_COL  полный апдейт без ребута"
        "onptar $M_COL Архивировать только текущую папку"
        "tarlist $M_COL Вывод файлов TAR"
        "onpuntar $M_COL Разархивировать"
        "onpupdatenodenpm $M_COL NPM update"
        "onpsrvall @ $M_COL ручная команда на серверах"
        "777noreb $M_COL апдейт на серверах"
        "888reb $M_COL фулл апгрейд на серверах"
        "mntall $M_COL присобачить флоппи"
        # "qwe $M_COL  выход"
        # "onplistrepo $M_COL  репозиторий обновлений"
        # "onpdrepo $M_COL  репозиторий обновлений"
        # "onpcat $M_COL показать файл профиля"
        # "onpufix $M_COL полное удаление багов репо"
        # "onpur $M_COL удаление багов репо"
        # "onpuu $M_COL апгрейд репо"
        # "onpu $M_COL простое обновление"
        )
    for i in "${help_arr_2[@]}"
    do
        echo -n "$H_COL $i"
        echo -n "   $D_COL/   "
    done
    echo -e ""
    echo -e "$TITLE_COL"



    echo "~~~~~~~ Список установочных настроек  ~~~~~~~"
    declare -a help_arr_3=(
        # "onpnano $M_COL  цвет консоли"
        # "onp_soft_install_soft_alni $M_COL  текст"
        "onp_soft_install_soft_alni $M_COL  установка моего всего софта"
        "onpcron $M_COL  проверка crontab"
        "onpallphp $M_COL  установка нужной версии PHP"
        "ALNI_VISIBLE_STORAGE_BOX_ONAPP_RSYNC_CHOWN $M_COL нужна для переноса файлов из StorageBox с изменением юзера и группы, например для WP"
        "ALNI_VISIBLE_FROM_STORAGE_BOX_RSYNC_NO_CHOWN $M_COL для переноса файлов из StorageBox папки на нужный сервер"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "command $M_COL command_text_here"
        # "app-unixbench $M_COL загрузка срипта"
        # "test_nench $M_COL загрузка срипта"
        # "app_mysql_read $M_COL чтение базы"
        # "app_mysql_rw_start $M_COL чтение/запись базы"
        # "app_mysql_rw_heavy $M_COL тяжелая нагрузка на базу"
        # "app_fileio $M_COL диск"
        # "app_ramio $M_COL память"
        # "app_cpuio $M_COL процессор"
        # "app_threads_1 $M_COL потоки быстро"
        # "app_threads_2 $M_COL потоки средне"
        # "app_threads_3 $M_COL потоки тяжелый"
    )
    for i in "${help_arr_3[@]}"
    do
        echo -n "$H_COL $i"
        echo -n "   $D_COL/   "
    done
    echo -e ""
    echo -e "$TITLE_COL"



    echo "~~~~~~~ Установка аппов  ~~~~~~~"
    declare -a help_arr_4=(
        # "inst-sysbench $M_COL тест sysbench"
        # "inst-webmin $M_COL webmin"
        # "inst-mysql $M_COL базы для теста"
        # "inst-onptest $M_COL тестовый пакет onp"
        "onpi $M_COL (ihtop | itest)"
        "onpapt $M_COL $1  ввести имя пакета"
    )
    for i in "${help_arr_4[@]}"
    do
        echo -n "$H_COL $i"
        echo -n "   $D_COL/   "
    done
    echo -e ""
    echo -e "$TITLE_COL"

    echo "~~~~~~~ Установка WWW среды ~~~~~~~"
    declare -a help_arr_5=(
        "onp_suit $M_COL весь набор разработчика"
        # "w_memcached_test $M_COL проверка мемкеша"
        "onpdummyping $M_COL установка ping.html для PING"
        "onpdummy_en $M_COL установка EN index.html EN"
        "onpdummy $M_COL установка EN index.html EN"
        "onpdummy_ru $M_COL установка RU index.html RU"
        "w_bitrix $M_COL файлы для битрикса"
        "onpvirtmin $M_COL  полная проверка virtmin"
        "webmincheck $M_COL  версии PHP webmin"
        "tradeconf $M_COL  общие настройки для th"
        "th $M_COL  help по th"
    )
    for i in "${help_arr_5[@]}"
    do
        echo -n "$H_COL $i"
        echo -n "   $D_COL/   "
    done
    echo -e ""
    echo -e ""
}
export -f onp-apps-help-list




onp-help-test() {
cat << EOF
        onpupdate
        onpd && inst-onptest

        sysbench fileio cleanup
        onptest_log_test
        onptest_log_crontab
EOF

}
export -f onp-help-test
