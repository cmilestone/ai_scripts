#!/bin/bash
# onp_alias
#
#
export onp_app_title="Файл с app......[OK]"
#source $HOME/onp/.onp_config.sh



onptar() {
    echo "~~~~~~~~~~~~~~~~ Версия 05_09_24   ~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~ Архивировать только каталог tar -zcvpf  file.tar.gz /full_path/ ~~~~~~~~~~~~~~~~~~"
    DIR_01_IN=$(pwd | sed 's,[^/]*$,,')
    DIR_02_WORD=$(pwd | sed 's#.*/##')
    DIR_03_FULL=$(pwd)
    DIR_04_DATE="`date +%Y_%m_%d____%H_%M`___"
    echo "~~~~~~~~~~~~~~~~~~ Создание архива $DIR_04_DATE$DIR_02_WORD.tar.gz папки $DIR_03_FULL/  в рабочей папке $DIR_01_IN ~~~~~~~~~~~~~~~~~~~~"
    # FILE_04_WHAT_TO_TAR=$(ls . | grep -v $DIR_02_WORD.tar*)
    cd $DIR_01_IN
    tar -czvpf $DIR_04_DATE$DIR_02_WORD.tar.gz $DIR_02_WORD
    mv $DIR_04_DATE$DIR_02_WORD.tar.gz $DIR_03_FULL
    cd $DIR_03_FULL
    ll | grep $DIR_04_DATE$DIR_02_WORD.tar*
    echo "~~~~~~~~~~~~~~~~~~ Тестирование архива ~~~~~~~~~~~~~~~~~~~~"
    gunzip -t $DIR_04_DATE$DIR_02_WORD.tar.gz
    echo "~~~~~~~~~~~~~~~~~~ Тестирование окончено ~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  $DIR_04_DATE$DIR_02_WORD.tar.gz завершено ~~~~~~~~~~~~~~~~~~~~"
    unset DIR_01_IN
    unset DIR_02_WORD
    unset DIR_03_FULL
    # unset FILE_TAR
    # rm alni__*
}
export -f onptar





onptarfull() {
    echo "~~~~~~~~~~~~~~~~ Версия tar full to par2 0.1-10-2022   ~~~~~~~~~~~~~~~~~~"
    DIR_00_cmd=sudo
    DIR_01_IN=$(pwd | sed 's,[^/]*$,,')
    DIR_02_WORD=$(pwd | sed 's#.*/##')
    DIR_03_FULL=$(pwd)
    DIR_04_SOLID=$(pwd | cut -d/ -f1-2)
    [ -d $DIR_03_FULL/_tar ] || $DIR_00_cmd mkdir $DIR_03_FULL/_tar
    [ -d $DIR_03_FULL/_tar/diff ] || $DIR_00_cmd mkdir $DIR_03_FULL/_tar/diff
    DIR_04_DATE="__`date +%d_%m_%Y_%H_%M`_full"
    echo "~~~~~~~~~~~~~~~~~~ Создание архива $DIR_02_WORD$DIR_04_DATE.tar ~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~ папки $DIR_03_FULL/  в рабочей папке $DIR_04_SOLID ~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~ в рабочей папке $DIR_04_SOLID ~~~~~~~~~~~~~~~~~~~~"
    cd $DIR_04_SOLID
    $DIR_00_cmd tar --exclude=_tar/*  --create -vf $DIR_02_WORD$DIR_04_DATE.tar   $DIR_03_FULL/.
    # https://axelstudios.github.io/7z/#!/
    $DIR_00_cmd 7z a -t7z $DIR_04_SOLID/$DIR_02_WORD$DIR_04_DATE.tar.7z -m0=lzma2 -mx9 -md256m -aoa $DIR_04_SOLID/$DIR_02_WORD$DIR_04_DATE.tar
    # 7z a -t7z Files.7z -m0=lzma2 -mx9 -md256m -v8128m -aoa $DIR_02_WORD$DIR_04_DATE.tar
    $DIR_00_cmd mv $DIR_04_SOLID/$DIR_02_WORD$DIR_04_DATE.tar.7z $DIR_03_FULL/_tar/$DIR_02_WORD$DIR_04_DATE.tar.7z
    ls $DIR_03_FULL/_tar/* | grep full
    echo "~~~~~~~~~~~~~~~~~~ Тестирование архива ~~~~~~~~~~~~~~~~~~~~"
    $DIR_00_cmd 7z t $DIR_03_FULL/_tar/$DIR_02_WORD$DIR_04_DATE.tar.7z
    echo "~~~~~~~~~~~~~~~~~~ Тестирование окончено ~~~~~~~~~~~~~~~~~~~~"
    # par2 c $DIR_03_FULL/_tar/$DIR_02_WORD$DIR_04_DATE.tar.7z
    echo "~~~~~~~~~~~~~~~~~~ Создаем PAR2 ~~~~~~~~~~~~~~~~~~~~"
    par2create -r100 -v -T10 $DIR_03_FULL/_tar/$DIR_02_WORD$DIR_04_DATE.tar.7z
    echo "~~~~~~~~~~~~~~~~~~ Проверяем PAR2 ~~~~~~~~~~~~~~~~~~~~"
    par2verify $DIR_03_FULL/_tar/$DIR_02_WORD$DIR_04_DATE.tar.7z
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  $DIR_02_WORD$DIR_04_DATE.tar завершено ~~~~~~~~~~~~~~~~~~~~"
    $DIR_00_cmd rm $DIR_02_WORD$DIR_04_DATE.tar
    cd $DIR_03_FULL
    unset DIR_01_IN
    unset DIR_02_WORD
    unset DIR_03_FULL
    # unset FILE_TAR
    # rm alni__*
}
export -f onptarfull



onptotar(){
    echo "~~~~~~~~~~~~~~~~ Версия prepare to tar 0.1-10-2022  Создание условий для TAR и par2 ~~~~~~~~~~~~~~~~~~"
    DIR_01_IN=$(pwd | sed 's,[^/]*$,,')
    DIR_02_WORD=$(pwd | sed 's#.*/##')
    DIR_03_FULL=$(pwd)
    [ -d $DIR_03_FULL/_tar ] || mkdir $DIR_03_FULL/_tar
    [ -d $DIR_03_FULL/_tar/diff ] || mkdir $DIR_03_FULL/_tar/diff
    echo $DIR_03_FULL
    ls -d */ && ls _tar/diff -d
    unset DIR_01_IN
    unset DIR_02_WORD
    unset DIR_03_FULL

}
export -f onptotar



tarlist() {
    echo "~~ Листинг архивов: ~~"
    VAR_TAR_LIST=$(ls | grep tar*.gz)
    ls | grep tar*.gz
    echo ""
    echo ""
    echo "~~ Сделать архив tar -zcvpf  file.tar.gz  /full_path/    |  Разархивирование файла: ~~"
    echo ""
    echo "tar -xzvpf $VAR_TAR_LIST"
    unset VAR_TAR_LIST
}
export -f tarlist


onpuntar() {
    ll | grep *tar*
    echo "~~~~~~~~~~~~~~~~~~ Введите имя архива:  ~~~~~~~~~~~~~~~~~~~~"
    read VAR_TAR_INPUT
    tar -xzvpf $VAR_TAR_INPUT
    echo "~~~~~~~~~~~~~~~~~~ Разархивирование файла $VAR_TAR_INPUT завершено ~~~~~~~~~~~~~~~~~~~~"
    unset VAR_TAR_INPUT
    ll
}
export -f onpuntar

onpunzip() {
    ll | grep *zip*
    echo "~~~~~~~~~~~~~~~~~~ Введите имя архива ZIP:  ~~~~~~~~~~~~~~~~~~~~"
    read VAR_TAR_INPUT
    unzip -a $VAR_TAR_INPUT
    echo "~~~~~~~~~~~~~~~~~~ Разархивирование файла $VAR_TAR_INPUT завершено ~~~~~~~~~~~~~~~~~~~~"
    unset VAR_TAR_INPUT
    ll
}
export -f onpunzip

onpuntargz() {
    echo "~~~~~~~~~~~~~~~~~~ Введите имя архива:  ~~~~~~~~~~~~~~~~~~~~"
    ll | grep *gz
    read VAR_TAR_INPUT
    echo "~~~~~~~~~~~~~~~~~~ Введите деректорию полного вида  ~~~~~~~~~~~~~~~~~~~~"
    pwd
    read VAR_DIR_INPUT
    tar -zxvf filename.sql.gz $VAR_TAR_INPUT --directory $VAR_DIR_INPUT
    echo "~~~~~~~~~~~~~~~~~~ Разархивирование файла $VAR_TAR_INPUT завершено ~~~~~~~~~~~~~~~~~~~~"
    unset VAR_TAR_INPUT
    unset VAR_DIR_INPUT
    ll
}
export -f onpuntargz




onpmysqldump() {
    echo "~~~~~~~~~~~~~~~~ Версия 29_07_15? pfgecr только из public_html  ~~~~~~~~~~~~~~~~~~"
    DIR_01_IN=$(pwd | sed 's,[^/]*$,,')
    DIR_02_WORD=$(pwd | sed 's#.*/##')
    DIR_03_FULL=$(pwd)
    DIR_04_DATE="`date +%Y_%m_%d__%H_%M`__"
    DIR_05_DEVIDER="____db"
    # FILE_04_WHAT_TO_TAR=$(ls . | grep -v $DIR_02_WORD.tar*)
    echo "~~~~~~~~~~~~~~~~ Введите юзера базы ~~~~~~~~~~~~~~~~"
    read ONP_MYSQL_USER
    echo "~~~~~~~~~~~~~~~~ Введите имя базы ~~~~~~~~~~~~~~~~"
    read ONP_MYSQL_BASENAME
    # echo "~~~~~~~~~~~~~~~~ Введите имя базы ~~~~~~~~~~~~~~~~"
    # read ONP_MYSQL_BASE_NAME
    echo "~~~~~~~~~~~~~~~~ Введите пароль базы ~~~~~~~~~~~~~~~~"
    read ONP_MYSQL_BASE_PASSWD
    echo " Создание архива  базы  $DIR_04_DATE$ONP_MYSQL_BASENAME.tar.gz папки $DIR_03_FULL/  в рабочей папке $DIR_01_IN"
    cd $DIR_01_IN

    mysqldump -h localhost -u $ONP_MYSQL_USER -p$ONP_MYSQL_BASE_PASSWD $ONP_MYSQL_BASENAME | gzip -9 > $DIR_04_DATE$ONP_MYSQL_BASENAME$DIR_05_DEVIDER.sql.gz

    # mv $DIR_04_DATE___$DIR_02_WORD_____db.sql.gz $DIR_03_FULL
    ls | grep $DIR_04_DATE$DIR_02_WORD$DIR_05_DEVIDER*
    cd $DIR_03_FULL
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  базы $DIR_04_DATE$ONP_MYSQL_BASENAME$DIR_05_DEVIDER.sql.gz завершено ~~~~~~~~~~~~~~~~~~~~"
    unset DIR_01_IN
    unset DIR_02_WORD
    unset DIR_03_FULL
    unset DIR_05_DEVIDER
    unset ONP_MYSQL_USER
    unset ONP_MYSQL_BASENAME
    unset ONP_MYSQL_BASE_PASSWD
}
export -f onpmysqldump




onpdarh() {
    echo "~~~~~~~~~~~~~~~~~~ Команды DAR ~~~~~~~~~~~~~~~~~~~~"
    echo "команда onplocaldar  ='dar -c my_backup4 -R / -g usr  -v -zgzip:6 -s:700M'"
    echo "команда onpunzip   ='dar -c my_backup4 -R / -g usr  -v -zgzip:6 -s:700M'"
    echo "dar -c onptest_log_a.log zip1 -v -zgzip:6 -s 720M"
cat << EOF
    -c  creates an archive
    -x  extracts files from the archive
    -d  compares the archive with the existing filesystem
    -t  tests the archive integrity
    -l  lists the contents of the archive
    -C  isolates the catalogue from an archive
    -+  merge two archives / create a sub archive
    -y  repair a truncated archive
EOF

}
export -f onpdarh


onplocdar() {
    echo "~~~~~~~~~~~~~~~~~~ Создание архива по 720MB ~~~~~~~~~~~~~~~~~~~~"
    ARCHIVE_NAME_1=test
    PATH_2=*
    #dar -c $ARCHIVE_NAME_1 -R $PATH_2 -v -zgzip:6 -s720M
    dar -c $PATH_2 -R / -g $ARCHIVE_NAME_1 -v -zgzip:6
    echo "~~~~~~~~~~~~~~~~~~ Создание архива завершено успешно ~~~~~~~~~~~~~~~~~~~~"
    ls -l -h | grep $ZIP_NAME_2
    unset ZIP_NAME_1
    unset ZIP_NAME_2
}
export -f onplocdar



onplocundar() {
    echo "~~~~~~~~~~~~~~~~~~ Создание архива по 720MB ~~~~~~~~~~~~~~~~~~~~"
    #dar -x $1 -R / -g $2 -v -zgzip:6 -s720M
    dar -x $1 -R / -g $2 -v -zgzip:6
    echo "~~~~~~~~~~~~~~~~~~ Создание архива завершено успешно ~~~~~~~~~~~~~~~~~~~~"
    ls
}
export -f onplocundar


onplocdarn() {
    echo "~~~~~~~~~~~~~~~~~~ Создание архива ~~~~~~~~~~~~~~~~~~~~"
    ls
    echo "Введите путь или файл для архивирования"
    read ZIP_NAME_1
    echo "Введите имя архива для архивирования"
    read ZIP_NAME_2
    echo "Введите размер архива в MB"
    read ZIP_NAME_3
    dar -c $ZIP_NAME_1 -R / -g $ZIP_NAME_2 -v -zgzip:6 -s$ZIP_NAME_3M
    dar -c $ZIP_NAME_1 -R / -g $ZIP_NAME_2 -v -zgzip:6 -s$ZIP_NAME_3M
    echo "~~~~~~~~~~~~~~~~~~ Создание архива завершено успешно ~~~~~~~~~~~~~~~~~~~~"
    ls -l -h | grep $ZIP_NAME_2
    unset ZIP_NAME_1
    unset ZIP_NAME_2
    unset ZIP_NAME_3
}
export -f onplocdarn



onploctarz() {
    echo "~~~~~~~~~~~~~~~~~~ Введите тип архива [gz], [bz2], [lz] или [xz] ~~~~~~~~~~~~~~~~~~~~"
    read VAR_TAR
    DIR_TAR=$(pwd | sed 's#.*/##')
    echo "~~~~~~~~~~~~~~~~~~ Создание архива формата $VAR_TAR в папке $DIR_TAR ~~~~~~~~~~~~~~~~~~~~"
    FILE_TAR=$(ls . | grep -v *.$VAR_TAR)
    tar -cavf $DIR_TAR.tar.$VAR_TAR $FILE_TAR
    rm -r $FILE_TAR
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  завершено ~~~~~~~~~~~~~~~~~~~~"
    ls | grep $VAR_TAR
    unset VAR_TAR
    unset DIR_TAR
    unset FILE_TAR
}
export -f onploctarz


onploctar() {
    VAR_TAR=tar
    DIR_TAR=$(pwd | sed 's#.*/##')
    echo "~~~~~~~~~~~~~~~~~~ Создание архива формата $VAR_TAR в папке $DIR_TAR ~~~~~~~~~~~~~~~~~~~~"
    FILE_TAR=$(ls . | grep -v *.$VAR_TAR)
    tar -cvf $DIR_TAR.tar $FILE_TAR
    rm -r $FILE_TAR
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  завершено ~~~~~~~~~~~~~~~~~~~~"
    ls | grep $VAR_TAR
    unset VAR_TAR
    unset DIR_TAR
    unset FILE_TAR
}
export -f onploctar


onploctar7z() {
    # echo "~~~~~~~~~~~~~~~~~~ Введите точный тип архива [7z], [gzip], [bzip2], [xz] ~~~~~~~~~~~~~~~~~~~~"
    VAR_TAR=7z
    DIR_TAR=$(pwd | sed 's#.*/##')
    echo "~~~~~~~~~~~~~~~~~~ Создание архива формата $VAR_TAR в папке $DIR_TAR ~~~~~~~~~~~~~~~~~~~~"
    FILE_TAR=$(ls . | grep -v *.$VAR_TAR)
    tar cf - $FILE_TAR  |  7za  a -si -t$VAR_TAR $DIR_TAR.$VAR_TAR
    tar cf - $FILE_TAR  |  rar  a si $VAR_TAR $DIR_TAR.rar
    rm -r $FILE_TAR
    echo "~~~~~~~~~~~~~~~~~~ Создание архива  завершено ~~~~~~~~~~~~~~~~~~~~"
    ls | grep $VAR_TAR
    unset VAR_TAR
    unset DIR_TAR
    unset FILE_TAR

}
export -f onploctar7z



onplocun7z() {
    UNTAR_NAME=$(ls | grep tar)
    echo "~~~~~~~~~~~~~~~~~~  Хотите распаковать архив $UNTAR_NAME? [y/n] ~~~~~~~~~~~~~~~~~~~~"
    read UNTAR_OK
    if [ $UNTAR_OK = "y"  ]; then
        7za x -so $UNTAR_NAME | tar -xvf -
        echo "~~~~~~~~~~~~~~~~~~ Архив $UNTAR_NAME успешно распакован ~~~~~~~~~~~~~~~~~~~~"
    else
        echo "~~~~~~~~~~~~~~~~~~ OK, до новых встреч ~~~~~~~~~~~~~~~~~~~~"
    fi
    unset UNTAR_NAME
}
export -f onplocun7z


onplocuntar() {
    UNTAR_NAME=$(ls | grep *.tar)
    echo "~~~~~~~~~~~~~~~~~~ Хотите распаковать архив $UNTAR_NAME? [y/n] ~~~~~~~~~~~~~~~~~~~~"
    read UNTAR_OK
    if [ $UNTAR_OK = "y"  ]; then
        tar -xvf $UNTAR_NAME
        echo "~~~~~~~~~~~~~~~~~~ Архив $UNTAR_NAME успешно распакован ~~~~~~~~~~~~~~~~~~~~"
    else
        echo "~~~~~~~~~~~~~~~~~~ OK, до новых встреч ~~~~~~~~~~~~~~~~~~~~"
    fi
    unset UNTAR_NAME

}
export -f onplocuntar



onp7z_test() {
    UNTAR_NAME=$(ls | grep *)
    echo "~~~~~~~~~~~~~~~~~~ Протестировать архив $UNTAR_NAME? [y/n] ? ~~~~~~~~~~~~~~~~~~~~"
    read UNTAR_OK
    if [ $UNTAR_OK = "y"  ]; then
        7z a t $UNTAR_NAME
        echo "~~~~~~~~~~~~~~~~~~ Архив $UNTAR_NAME успешно протестирван ~~~~~~~~~~~~~~~~~~~~"
    else
        echo "~~~~~~~~~~~~~~~~~~ OK, до новых встреч ~~~~~~~~~~~~~~~~~~~~"
    fi
    unset UNTAR_NAME
}
export -f onp7z_test



onppar() {
    PAR_NAME=$(ls | grep *)
    echo "~~~~~~~~~~~~~~~~~~ Добавить информацию восстановления для $PAR_NAME? [y/n] ? ~~~~~~~~~~~~~~~~~~~~"
    read PAR_OK
    if [ $PAR_OK = "y"  ]; then
        par2 c -r10 $PAR_NAME
        echo "~~~~~~~~~~~~~~~~~~ Информация восстановления для $PAR_NAME успешно создана ~~~~~~~~~~~~~~~~~~~~"
    else
        echo "~~~~~~~~~~~~~~~~~~ OK, до новых встреч ~~~~~~~~~~~~~~~~~~~~"
    fi
    unset PAR_NAME
}
export -f onppar