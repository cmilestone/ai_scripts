#!/bin/bash
# Debian start install
#
#

# curl -s https://gitlab.com/cmilestone/ai_scripts/raw/master/.config_urls > $HOME/onp/.config_urls && wget -i $HOME/onp/.config_urls -N -q

#Проверка всех загрузок
source $HOME/.onp_alias
# source $HOME/onp/.onp_soft.sh
# source $HOME/onp/.onp_os.sh
        ___onp_config_setup_line

#Лог времени
echo "Обновляем систему"
onpdf
#Добавляем crontab
echo "Добавляем crontab"
onp_addonp_crontab
        ______onp_config_log_line

#Ставим общий сетап
echo "Ставим общий сетап"
onp_config_cli_color
        ___onp_config_setup_line

#Обновляем
onp_soft_full_remove_and_all_update
        ___onp_config_setup_line

####################################################################################################################
#Ставим локаль Debian
onp_os_debian_locale
        ___onp_config_setup_line
####################################################################################################################


#Диск подкачки
onp_config_disk_swap
        ___onp_config_setup_line

#Ставим время
onp_config_time
        ___onp_config_setup_line

#Общие программы
onp_soft_install_soft_alni
onpuu
onp_soft_install_soft_alni
onpuu
onp_soft_install_soft_alni
        ___onp_config_setup_line



#Простое обновление
onp_soft_simple_update
        ___onp_config_setup_line

# Лог времени
        ______onp_config_log_line


#Консоль цвета
onpnano

#Закладки
onpbashmarks

#PHP
onp_multiphp_debian

#AWS s3
# onp_awscli

onp_add_onp_bash

# Ставим WEBMIN
echo "###################### Установка WEBMIN Apache2      |       onp_server_lamp ######################"
echo "###################### Установка WEBMIN Nginx      |      onp_server_nginx ######################"

#Команды далее
onp_config_end_server_help
        ___onp_config_setup_line



#окончание
onp_config_end_update
        ___onp_config_setup_line


