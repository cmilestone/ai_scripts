#!/bin/bash
# onp_alias
#
#

onppass(){
cat << EOF
#~~~~~~~~~~   ONP pass ~~~~~~~~~~~
source $HOME/.ssh/.onp_pass.sh
ONPCONF_REDIS="x"
ONPCONF_MEMCHACHED="x"
nano $HOME/.ssh/.onp_pass.sh
redis-benchmark -q -n 1000 -c 10 -P 5
redis-cli -a $ONPCONF_REDIS monitor
redis-cli -a __pass_here_ monitor
EOF

}
export -f onppass

onpconf(){
echo "~~~~~~~~~~   Команды для конфигов ~~~~~~~~~~~"
echo ""
echo ""
source $HOME/.ssh/.onp_pass.sh
cat << EOF
############################################################################
ЧТО ДЕЛАТЬ ПОСЛЕ УСТАНОВКИ СЕРВЕРА
############################################################################

############################################################################
#~~~~~~~~~~   UBUNTU REPO   ~~~~~~~~~~~
###### Main Ubuntu 20.04
deb http://mirror.yandex.ru/ubuntu/ focal main
deb-src http://mirror.yandex.ru/ubuntu/ focal main
deb http://mirror.yandex.ru/ubuntu/ focal-updates main restricted

deb http://mirror.yandex.ru/ubuntu/ focal universe
deb http://mirror.yandex.ru/ubuntu/ focal-updates universe

deb http://mirror.yandex.ru/ubuntu/ focal multiverse
deb http://mirror.yandex.ru/ubuntu/ focal-updates multiverse

deb http://mirror.yandex.ru/ubuntu/ focal-backports main restricted universe multiverse

deb http://mirror.yandex.ru/ubuntu/ focal-security main restricted
deb http://mirror.yandex.ru/ubuntu/ focal-security universe
deb http://mirror.yandex.ru/ubuntu/ focal-security multiverse
#~~~~~~~~~~   UBUNTU REPO   ~~~~~~~~~~~
############################################################################

~~~~~~~~~~   01 pubkey  config ~~~~~~~~~~~
ssh-copy-id username@remote_host -p xxx


~~~~~~~~~~   02 ssh  config ~~~~~~~~~~~
mkdir -p ~/.ssh && chmod 700 ~/.ssh && ssh-keygen -t rsa
>>>>>>>> nano /etc/ssh/sshd_config
изменить port
service ssh restart && service sshd restart
exit

~~~~~~~~~~   02 ONP onp_server_evernote_install ~~~~~~~~~~~
>>>>>>>> onp_server_evernote_install

~~~~~~~~~~   03 Virtualmin ~~~~~~~~~~~
>>>>>>>> установка virtualmin apache
onp_server_lamp

~~~~~~~~~~   04 проверка софта  ~~~~~~~~~~~
>>>>>>>> после reboot  проверить
onp_soft_install_soft_alni

>>>>>>>> Для мульти PHP
onp_multiphp_debian

~~~~~~~~~~   REDIS config ~~~~~~~~~~~
nano /etc/redis/redis.conf

####### ONP ########
        maxmemory 1024mb
        maxmemory-policy allkeys-lru
        requirepass pass_here
####################

rest redis
redis-benchmark -q -n 1000 -c 10 -P 5
redis-cli -a $ONPCONF_REDIS monitor


~~~~~~~~~~   Memcached  config ~~~~~~~~~~~
nano /etc/memcached.conf
    >>>>>>>> Увеличить память  1024/2048/4096

-m 1024

rest memcached


                 ############################################################################
                        ~~~~~~~~~~   varnish-->apache2  config ~~~~~~~~~~~
                        ~~~~~~~~~~   varnish config ~~~~~~~~~~~
                                                    >>>>>>>> Порт сервера varnish 80:443

                                                    >>>>>>>> http://agiletesting.blogspot.com/2017/06/ssl-termination-and-http-caching-with.html
                                                    onpapt varnish
                                                    netstat -plntu | grep 'varnishd\|apache2\|haproxy'
                                                    stop apache2 haproxy varnish
                                                    netstat -plntu | grep 'varnishd\|apache2\|haproxy'


                        ~~~~~~~~~~   Varnish config ~~~~~~~~~~~
                        >>>>>>>> Порт сервера Varnish 8888
                        stop apache2 varnish
                        netstat -plntu | grep 'varnishd\|apache2'


                        >>Порт, который слушает VARNISH :80 >>>>>>>>>>
                        nano /lib/systemd/system/varnish.service
                                                                    ExecStart=/usr/sbin/varnishd -j unix,Cuser=vcache -F -a :80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,1024m

                        ##################### >>Порт, который слушает VARNISH :80 >>>>>>>>>>
                        ##################### nano /etc/systemd/system/varnish.service
                                                                    ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F -a :80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,1024m
                        >>Порт, который слушает  VARNISH :80 >>>>>>>>>>
                        nano /etc/default/varnish
                                                                        DAEMON_OPTS="-a :80 \
                                                                                -T localhost:6082 \
                                                                                -f /etc/varnish/default.vcl \
                                                                                -S /etc/varnish/secret \
                                                                                -s malloc,1024m"

                        >>КУДА АДРЕСОВАТЬ VARNISH + BIND (указывать только полный айпи)>>>>>>>>>>
                        nano /etc/varnish/default.vcl
                                                                    # .host = "127.0.0.1";
                                                                    .host = "212.109.195.70";
                                                                    .port = "8888";
                        systemctl stop varnish.service
                        systemctl daemon-reload
                        systemctl start varnish.service && rest varnish
                        rjurn varnish
                        rall
                        onprun

                        ~~~~~~~~~~   apache2 config ~~~~~~~~~~~
                        >>>>>>>> Меняем порт сервера apache2 8888
                        nano /etc/apache2/ports.conf
                                                    ##### ONP HighLOAD
                                                    Listen 8009

                        systemctl enable varnish && systemctl enable apache2
                        start apache2 varnish
                        rest apache2  varnish
                        netstat -plntu | grep 'varnishd\|apache2\|haproxy'

                        start apache2 varnish
                        netstat -plntu | grep 'varnishd\|apache2\|haproxy'




WP SSL VARNISH
>>>>>>>>>>>>>>>>> wp_config
define('FORCE_SSL_ADMIN', true);

if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
        $_SERVER['HTTPS']='on';
>>>>>>>>>>>>>>>>> 
VARNISH APACHE
https://medium.com/@sivaschenko/apache-ssl-termination-https-varnish-cache-3940840531ad
https://bash-prompt.net/guides/apache-varnish/

sudo a2enmod ssl
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod proxy
sudo a2enmod proxy_balancer
sudo a2enmod proxy_http

>>>>>>>>>>>>>>>>> 
>>>>>>>>>>>>>>>>> 
############################################################################
                        ~~~~~~~~~~   DEBIAN  config ~~~~~~~~~~~
                        nano /etc/apt/sources.list

############################################################################
~~~~~~~~~~   postfix  config ~~~~~~~~~~~
>>>>>>>> sudo dpkg-reconfigure postfix
Интернет-сайт
>>>>>>>>  nano /etc/postfix/main.cf
inet_interfaces = loopback-only
mailbox_command = /usr/bin/procmail-wrapper -o -a $DOMAIN -d $LOGNAME


>>>>>>>> nano /etc/aliases
# /etc/aliases
mailer-daemon: postmaster
postmaster: root
nobody: root
hostmaster: root
usenet: root
news: root
webmaster: root
www: root
ftp: root
abuse: root
noc: root
security: root
clamav: root
root: server_name_here@onapp.ru


>>>>>>>>  sudo newaliases && rest postfix


~~~~~~~~~~   fail2ban  config ~~~~~~~~~~~

ipset list
systemctl enable fail2ban
    >>>>>>>> nano /etc/fail2ban/jail.local
    action = firewallcmd-ipset для  всех + DEFAULT rule + custom
            [jail_name]
            enabled = true
            port    = xxx
            findtime = 36000
            maxretry = 4
            bantime  = 864000
            action = firewallcmd-ipset
            enabled = true
                                findtime = 72000
                                maxretry = 3
                                bantime  = 864000
                                action = firewallcmd-ipset

    rsrv



~~~~~~~~~~   firewalld  config ~~~~~~~~~~~
systemctl status firewalld -l
sudo firewall-cmd --state
                        sudo firewall-cmd --set-default-zone=public


                        iptables-save > /etc/iptables.up.rules
                        ip6tables-save > /etc/ip6tables.up.rules


                        sudo firewall-cmd --add-service="dhcpv6-client" --permanent && sudo firewall-cmd --add-service="ftp" --permanent && sudo firewall-cmd --add-service="http" --permanent && sudo firewall-cmd --add-service="https" --permanent && sudo firewall-cmd --add-service="imap" --permanent && sudo firewall-cmd --add-service="imaps" --permanent && sudo firewall-cmd --add-service="pop3" --permanent && sudo firewall-cmd --add-service="pop3s" --permanent && sudo firewall-cmd --add-service="smtp" --permanent && sudo firewall-cmd --add-service="smtp" --permanent && sudo firewall-cmd --add-service="smtp" --permanent && sudo firewall-cmd --add-service="smtps" --permanent && sudo firewall-cmd --add-service="ssh" --permanent && firewall-cmd --reload && sudo firewall-cmd --set-default-zone=public

                        firewall-cmd --permanent --zone=public --add-port=587/tcp && firewall-cmd --permanent --zone=public --add-port=53/tcp && firewall-cmd --permanent --zone=public --add-port=20/tcp && firewall-cmd --permanent --zone=public --add-port=21/tcp && firewall-cmd --permanent --zone=public --add-port=2222/tcp && firewall-cmd --permanent --zone=public --add-port=10000-10100/tcp && firewall-cmd --permanent --zone=public --add-port=20000/tcp && firewall-cmd --permanent --zone=public --add-port=1025-65535/tcp && firewall-cmd --permanent --zone=public --add-port=53/udp  && firewall-cmd --reload && sudo firewall-cmd --set-default-zone=public

                        cd ~ && sudo iptables-save > iptables-export && sudo ip6tables-save > ip6tables-export
                        далее на другом серваке
                        sudo iptables-restore < ~/iptables-export && sudo ip6tables-restore < ~/ip6tables-export
                        iptables-restore-translate -f ~/iptables-export > nft_ruleset.nft

                        ------

                        sudo systemctl enable --now firewalld
                            firewall-cmd --list-all
                            systemctl status firewalld -l
                            firewall-cmd --permanent --zone=public --add-port=10000/tcp

                            update-alternatives --set iptables /usr/sbin/iptables-legacy
                        ПОРТЫ
                            ADD
                            firewall-cmd --permanent --zone=public --add-port=10000/tcp
                            sudo firewall-cmd --add-port 1622/tcp --permanent
                            firewall-cmd --zone=public --add-port=21/tcp
                            sudo firewall-cmd --reload

                            REMOVE
                            sudo firewall-cmd --remove-port 1622/tcp --permanent



                        ~~~~~~~~~~   proftpd  config ~~~~~~~~~~~
                        proftpd --configtest

                        ssh-keygen -m PEM -f /etc/proftpd/ssh_host_rsa_key -N '' -t rsa -b 2048
                        ssh-keygen -m PEM -f /etc/proftpd/ssh_host_dsa_key -N '' -t dsa -b 1024
                        ssh-keygen -m PEM -f /etc/proftpd/ssh_host_ecdsa_key -N '' -t ecdsa -b 521

                        >>>>>>>>>>>>> BUSTER proftpd 1.36
                        # rm /etc/ssh/ssh_host*
                        # ssh-keygen -A -m PEM



                        ~~~~~~~~~~   Timeshift DUMP  ~~~~~~~~~~~
                        timeshift --list-devices
                        >>>>>>>>>>>>>>>>>>>>>> timeshift --snapshot-device /dev/vda1/
                        nano /etc/timeshift.json
                        timeshift --create --comments "01 Перед обновлением"
                        timeshift --list
                        ############################################################################
                        ~~~~~~~~~~   ВСЕ СЛУЖБЫ  ~~~~~~~~~~~
                        systemctl enable name / systemctl disable name
                        systemctl mask name / systemctl unmask name
                        systemctl start name / systemctl stop name
                        systemctl restart name / systemctl status name
                        ############################################################################

############################################################################
~~~~~~~~~~   Root МОДИФИКАЦИЯ  ~~~~~~~~~~~
    >>>>>>>> sudo visudo
    user    ALL=(ALL) NOPASSWD:ALL

~~~~~~~~~~   APACHE2  config ~~~~~~~~~~~
cd /etc/apache2
nano /etc/apache2/ports.conf
nano /etc/php/00-onapp-custom.ini


onpinifpm


############################################################################
~~~~~~~~~~   MOUNT VDB ~~~~~~~~~~~
lsblk
sudo mkfs.ext4 /dev/vdb
sudo mkdir /mnt/vdb

mount -o barrier=0 /dev/vdb /mnt/vdb



sudo nano /etc/fstab
>>>>>>>>>>>>>>>>>>>> /dev/vdb    /mnt/vdb    ext4    defaults    0 0
/dev/vdb    /mnt/vdb    ext4    defaults    0 0
sudo mount /mnt/vdb
reboot
df -h


~~~~~~~~~~   UNMOUNT VDB ~~~~~~~~~~~
df -h && lsblk
sudo nano /etc/fstab
reboot
sudo rm -r /mnt/vdb






############################################################################
AWS
https://objectivefs.com/howto/how-to-restrict-s3-bucket-policy-to-only-one-aws-s3-bucket


############################################################################
#Server speed
curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -



############################################################################
#свой ключ на сервер
mkdir -p ~/.ssh && chmod 700 ~/.ssh && ssh-keygen -t rsa
ssh-copy-id root@ip

nano /etc/ssh/sshd_config && service ssh restart && service sshd restart

############################################################################
## SSHFS
#https://blog.sedicomm.com/2017/11/10/kak-montirovat-udalennuyu-fajlovuyu-sistemu-ili-katalog-linux-s-pomoshhyu-sshfs-cherez-ssh/

onpapt sshfs
sshfs -o allow_other user@_IP_HERE_ :/mnt/sshfs/server_name/ /mnt/contabo -p port_here

sudo nano /etc/fstab
fuse.sshfs delay_connect,_netdev,user,IdentityFile=/root/.ssh/id_rsa,idmap=user,allow_other,default_permissions,port=PORTNUMBER


rsync -avz --no-i-r -e 'ssh -p _SOURCE_PORT_HERE_' --progress  --remove-source-files root@_SOURCE_IP_HERE_:/__SOURCE_DIR_/ /_DESTINATION_DIR_/



############################################################################
## autofs
onpapt autofs
все настройки в etc/auto.master.d



Ссылка 
ln -s <ИСТОЧНИК> <ИМЯ_ССЫЛКИ>


SUPERVISOR
sudo service supervisor restart && sudo supervisorctl restart name_superviser_app && sudo service nginx restart

############################################################################
~~~~~~~~~~   ЗАЩИТА VPS  ~~~~~~~~~~~
apt-get install vnstat vnstat -y
ifconfig
https://www.cyberciti.biz/faq/ubuntu-install-vnstat-console-network-traffic-monitor/




############################################################################
~~~~~~~~~~   fail2ban UNBAN  ~~~~~~~~~~~
>>>>>>>> fail2ban-client set JAIL_NAME unbanip ip_here
fail2ban-client set webmin-auth unbanip 5.18.100.154

cat /var/log/fail2ban.log | grep 5.18.100.154

############################################################################
~~~~~~~~~~   ТЮНИНГ  ~~~~~~~~~~~
MySQL
https://www.hostcms.ru/documentation/server/mysql/

www.conf file in the php-fpm pool
https://www.kinamo.be/en/support/faq/determining-the-correct-number-of-child-processes-for-php-fpm-on-nginx


# SESSION
; php_admin_value[session.save_handler] = memcached
; php_admin_value[session.save_path] = "127.0.0.1:11211"
php_admin_value[session.save_handler] = redis
# php_admin_value[session.save_path] = "tcp://127.0.0.1:6379?auth=Redis_PASSW__Timeweb123RedisPass2020&timeout=3&read_timeout=3&persistent=1&database=16&prefix=PREFIX_SESSION_ONP:"
php_admin_value[session.save_path] = "tcp://127.0.0.1:6379?auth=Redis_PASSW__Timeweb123RedisPass2020&timeout=3&read_timeout=3&persistent=1&database=16"
php_admin_value[session.gc_maxlifetime] = 259200

fail2ban-client set webmin-auth unbanip 5.18.96.155 






################### IHOR
 sudo supervisorctl stop fhlo.onapp.ru && virtualenv vpipenv && source vpipenv/bin/activate && pipenv install -r /var/www/lenhockey.ru/site/requirements.txt && deactivate && sudo service  supervisor restart && sudo supervisorctl restart fhlo.onapp.ru && sudo service nginx restart






################### LEMP Timeweb
sudo apt-get install python-dev python2-dev python3-dev build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev supervisor

################### LEMP Timeweb Python3
cd /home/fhlo_onapp_ru/public_html/lenhockey_ru/ && virtualenv vpipenv && source vpipenv/bin/activate && pipenv install -r /home/fhlo_onapp_ru/public_html/lenhockey_ru/site/requirements.txt && deactivate
&& sudo service  supervisor restart && sudo supervisorctl restart fhlo.onapp.ru && sudo service nginx restart


Pipenv


################### LEMP Timeweb Python2
cd /home/fhlo_onapp_ru/public_html/lenhockey_ru/ && virtualenv -p /usr/bin/python2.7 vpipenv && source vpipenv/bin/activate && pipenv --two install -r /home/fhlo_onapp_ru/public_html/lenhockey_ru/site/requirements.txt && deactivate
&& sudo service  supervisor restart && sudo supervisorctl restart fhlo.onapp.ru && sudo service nginx restart



python manage.py runserver


supervisorctl status | awk '{print $1, $2}'

############################################################################
~~~~~~~~~~   Однопользователский режим  ~~~~~~~~~~~
mount -o remount -w /

EOF
}
export -f onpconf



onpconfphp(){
echo "~~~~~~~~~~   Команды для конфигов ~~~~~~~~~~~"
echo ""
echo ""
source $HOME/.ssh/.onp_pass.sh
cat << EOF
############################################################################
/etc/php/8.1/fpm/php.ini=8.1  FPM
/etc/php/8.1/cgi/php.ini=8.1  for scripts run via CGI
/etc/php/8.1/cli/php.ini=8.1  cli
/etc/php/8.1/apache2/php.ini=8.1  for mod_php


/etc/php/8.0/fpm/php.ini=8.0  FPM
/etc/php/8.0/cgi/php.ini=8.0  for scripts run via CGI
/etc/php/8.0/cli/php.ini=8.0  cli
/etc/php/8.0/apache2/php.ini=8.0  for mod_php


/etc/php/7.4/fpm/php.ini=7.4  FPM
/etc/php/7.4/cgi/php.ini=7.4  for scripts run via CGI
/etc/php/7.4/cli/php.ini=7.4  cli
/etc/php/7.4/apache2/php.ini=7.4  for mod_php


/etc/php/7.3/fpm/php.ini=7.3  FPM
/etc/php/7.3/cgi/php.ini=7.3  for scripts run via CGI
/etc/php/7.3/cli/php.ini=7.3  cli
/etc/php/7.3/apache2/php.ini=7.3  for mod_php



/etc/php/7.2/fpm/php.ini=7.2  FPM
/etc/php/7.2/cgi/php.ini=7.2  for scripts run via CGI
/etc/php/7.2/cli/php.ini=7.2  cli
/etc/php/7.2/apache2/php.ini=7.2  for mod_php



/etc/php/7.1/fpm/php.ini=7.1  FPM
/etc/php/7.1/cgi/php.ini=7.1  for scripts run via CGI
/etc/php/7.1/cli/php.ini=7.1  cli
/etc/php/7.1/apache2/php.ini=7.1  for mod_php


/etc/php/7.0/fpm/php.ini=7.0  FPM
/etc/php/7.0/cgi/php.ini=7.0 cgi
/etc/php/7.0/cli/php.ini=7.0  cli
/etc/php/7.0/apache2/php.ini=7.0  for mod_php


/etc/php/5.6/fpm/php.ini=5.6  FPM
/etc/php/5.6/cgi/php.ini=5.6 cgi
/etc/php/5.6/cli/php.ini=5.6  cli
/etc/php/5.6/apache2/php.ini=5.6  for mod_php
EOF
}
export -f onpconfphp

onponpconftest_input(){
echo "~~~~~~~~~~   Команды onponpconftest ~~~~~~~~~~~"
echo ""
echo ""
cat << EOF
not this line
second line
START
A good line to include
And this line
~~~ DEBIAN ~~~
Yep
1
2
3
OK DEBIAN
END
Nope more

EOF
}
export -f onponpconftest_input


# TODO: onponpconftest Пока в процессе отдельной выборки того, чтобы onpconf по команде
onponpconftest(){
    VAR_TEST="~~~ $@ ~~~"
    VAR_TEST_ECHO=$(echo $VAR_TEST)
    onponpconftest_input | awk '/END/{found=0} {if(found) print} /~~~ hostname1=$VAR_TEST; print hostname1 ~~~/{found=1}'

    echo $VAR_TEST
    # $VAR_TEST_COMMAND  awk '{hostname1=$VAR_TEST_ECHO ; print hostname1 ; }'
    #onponpconftest_input | awk '/END/{found=0} {if(found) print} /~~~ $VAR_TEST ~~~/{found=1}'
    #unset VAR_TEST
}
export -f onponpconftest
