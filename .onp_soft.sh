#!/bin/bash
# onp_soft
#
#
source $HOME/onp/.onp_app_list.sh
export onp_soft_title="Файл с выбором программ......[OK]"



onp_soft_install_soft_alni() {
    echo "~~~~~~~~~~~~~~~~~~ Ставим ALNI ONP софт  ~~~~~~~~~~~~~~~~~~~~"
    #03 minimal
    onpapt_list_minimal

    #02 essential
    onpapt_list_essential
    sudo netstat -tulpn | grep LISTEN

    #01 Основной
    onpapt_list_main

    #04 Софт дополнительный Важный
    onpapt_list_roll_apps


    #Несколько PHP версий
    #onp_multiphp_debian
}
export -f onp_soft_install_soft_alni

onp_soft_install_soft_trade() {
    echo "~~~~~~~~~~~~~~~~~~ Ставим ALNI ONP софт  ~~~~~~~~~~~~~~~~~~~~"
    onp_add_repo_nginx
    #03 minimal
    onpapt_list_minimal

    #02 essential
    onpapt_list_essential_trade
    sudo netstat -tulpn | grep LISTEN

    #01 Основной
    onpapt_list_main_trade

    #04 Софт дополнительный Важный
    onpapt_list_roll_apps


    #Несколько PHP версий
    #onp_multiphp_debian
}
export -f onp_soft_install_soft_trade

onp_soft_simple_update () {
    #Простое обновление
    sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt autoremove
}
export -f onp_soft_simple_update


onp_soft_full_all_update () {
    #ТОлько обновление
    sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt full-upgrade -y && sudo apt-get update --fix-missing -y
}
export -f onp_soft_full_all_update


onp_soft_full_remove_and_all_update () {
    #Полное обновление
    sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt full-upgrade -y && sudo apt-get update -y && sudo apt autoremove -y && sudo apt-get update --fix-missing -y
    sudo dpkg --configure -a && sudo apt-get clean && sudo apt-get update
}
export -f onp_soft_full_remove_and_all_update



www_install_func () {
    # $PWD_="$PWD/"
    cp $WWW_INSTALL_FUNC_PATH$ONP_WWW_INSTALL_FILE $PWD/$ONP_WWW_INSTALL_FILE
    echo "Файл $ONP_WWW_INSTALL_FILE скопирован в каталог"
}
export -f www_install_func

onpbitrixsetup () {
    WWW_INSTALL_FUNC_PATH=$HOME/onp_www_apps/
    ONP_WWW_INSTALL_FILE=.bitrix_server_test.php
    www_install_func
    unset ONP_WWW_INSTALL_FILE
    ONP_WWW_INSTALL_FILE=.bitrixsetup.php
    www_install_func
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
}
export -f onpbitrixsetup


w_memcached_test () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_www_memcached.php
    WWW_INSTALL_FUNC_PATH=$HOME/onp_www_apps/
    www_install_func
    echo "memcache"
    echo "password"
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH

}
export -f w_memcached_test

onpdummy () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_index_dummy_en.html
    WWW_INSTALL_FUNC_PATH=$HOME/onp/
    www_install_func
    mv -f $PWD/$ONP_WWW_INSTALL_FILE $PWD/index.html
    echo "Файл index.html скопирован в каталог $PWD"
    ls | grep index.html
    DUMMY_01_SEARCH='ONAPP_REPLACE'
    DUMMY_02_IN=$(pwd | awk -F'/' '{print $(NF-1)}')
    DUMMY_03_OUT=$(echo $DUMMY_02_IN | sed  "s|_|.|g")
    sed -i "s|$DUMMY_01_SEARCH|$DUMMY_03_OUT|g" index.html
    DUMMY_20_SEARCH='ONAPP_REP_NAME'
    DUMMY_20_OUT=$(hostname)
    sed -i "s|$DUMMY_20_SEARCH|$DUMMY_20_OUT|g" index.html
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpdummy


onpdummy_en () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_index_dummy_en.html
    WWW_INSTALL_FUNC_PATH=$HOME/onp/
    www_install_func
    mv -f $PWD/$ONP_WWW_INSTALL_FILE $PWD/index.html
    echo "Файл index.html скопирован в каталог $PWD"
    ls | grep index.html
    DUMMY_01_SEARCH='ONAPP_REPLACE'
    DUMMY_02_IN=$(pwd | awk -F'/' '{print $(NF-1)}')
    DUMMY_03_OUT=$(echo $DUMMY_02_IN | sed  "s|_|.|g")
    sed -i "s|$DUMMY_01_SEARCH|$DUMMY_03_OUT|g" index.html
    DUMMY_20_SEARCH='ONAPP_REP_NAME'
    DUMMY_20_OUT=$(hostname)
    sed -i "s|$DUMMY_20_SEARCH|$DUMMY_20_OUT|g" index.html
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpdummy_en

onpdummy_ru () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_index_dummy_ru.html
    WWW_INSTALL_FUNC_PATH=$HOME/onp/
    www_install_func
    mv -f $PWD/$ONP_WWW_INSTALL_FILE $PWD/index.html
    echo "Файл index.html скопирован в каталог $PWD"
    ls | grep index.html
    DUMMY_01_SEARCH='ONAPP_REPLACE'
    DUMMY_02_IN=$(pwd | awk -F'/' '{print $(NF-1)}')
    DUMMY_03_OUT=$(echo $DUMMY_02_IN | sed  "s|_|.|g")
    sed -i "s|$DUMMY_01_SEARCH|$DUMMY_03_OUT|g" index.html
    DUMMY_20_SEARCH='ONAPP_REP_NAME'
    DUMMY_20_OUT=$(hostname)
    sed -i "s|$DUMMY_20_SEARCH|$DUMMY_20_OUT|g" index.html
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpdummy_ru

onpdummyping () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_ping.html
    WWW_INSTALL_FUNC_PATH=$HOME/onp/
    www_install_func
    mv -f $PWD/$ONP_WWW_INSTALL_FILE $PWD/ping.html
    echo "Файл ping.html скопирован в каталог $PWD"
    ls | grep ping.html
    DUMMY_01_SEARCH='ONAPP_REPLACE'
    DUMMY_02_IN=$(pwd | awk -F'/' '{print $(NF-1)}')
    DUMMY_03_OUT=$(echo $DUMMY_02_IN | sed  "s|_|.|g")
    sed -i "s|$DUMMY_01_SEARCH|$DUMMY_03_OUT|g" ping.html
    # DUMMY_20_SEARCH='ONAPP_REP_NAME'
    # DUMMY_20_OUT=$(hostname)
    # sed -i "s|$DUMMY_20_SEARCH|$DUMMY_20_OUT|g" index.html
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpdummyping

onpcors () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_cors_htaccess
    WWW_INSTALL_FUNC_PATH=$HOME/onp/
    www_install_func
    mv -f $PWD/$ONP_WWW_INSTALL_FILE $PWD/.htaccess
    echo "Файл .htaccess скопирован в каталог $PWD"
    ls | grep .htaccess
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpcors

onpsslcron () {
    # onp_www_memcached
    ONP_WWW_INSTALL_FILE=.onp_ssl_cron.sh
    WWW_INSTALL_FUNC_PATH=/home/
    www_install_func
    mv -f $WWW_INSTALL_FUNC_PATH/$ONP_WWW_INSTALL_FILE $WWW_INSTALL_FUNC_PATH/ssl_certs_cron.sh
    echo "Файл ssl_certs_cron.sh скопирован в каталог"
    ls | grep $WWW_INSTALL_FUNC_PATH/ssl_certs_cron
    DUMMY_01_SEARCH='ONAPP_REPLACE'
    DUMMY_02_IN=$(pwd | awk -F'/' '{print $(NF-1)}')
    sed -i "s|$DUMMY_01_SEARCH|$DUMMY_02_IN|g" index.html
    unset ONP_WWW_INSTALL_FILE
    unset WWW_INSTALL_FUNC_PATH
    unset DUMMY_01_IN
    # nano $PWD/index.html

}
export -f onpsslcron


onpssl () {
    # https://serversforhackers.com/c/letsencrypt-with-haproxy
    echo "~~~~~~~~~~~~ Введите домен для сертификата ~~~~~~~~~~~~"
    read ONP_SSL_DOMAIN
    ONP_SSL_EMAIL=ssl@onapp.ru
    echo "~~~~~~~~~~~~ Получаем сертификат ~~~~~~~~~~~~"
    # sudo certbot certonly --standalone -d $ONP_SSL_DOMAIN --non-interactive --agree-tos --email $ONP_SSL_EMAIL --http-01-port=8888
    sudo certbot certonly --standalone -d $ONP_SSL_DOMAIN --non-interactive --agree-tos --email $ONP_SSL_EMAIL
    echo "~~~~~~~~~~~~ Выполнение запроса окончено ~~~~~~~~~~~~"
    # echo "~~~~~~~~~~~~ Копирую ключ для haproxy ~~~~~~~~~~~~"
    # cat /etc/letsencrypt/live/$ONP_SSL_DOMAIN/fullchain.pem /etc/letsencrypt/live/$ONP_SSL_DOMAIN/privkey.pem > /etc/ssl/private/$ONP_SSL_DOMAIN.pem
    # echo "~~~~~~~~~~~~ Ключ для haproxy скопирован, проверяю: ~~~~~~~~~~~~"
    # ls /etc/ssl/private/ | grep $ONP_SSL_DOMAIN.pem
    # echo "~~~~~~~~~~~~ Все ключи на сервере, проверяю: ~~~~~~~~~~~~"
    ls /etc/ssl/private/
    # service haproxy restart
    unset ONP_SSL_EMAIL
    unset ONP_SSL_DOMAIN
}
export -f onpssl

onpssllist(){
    ls /etc/nginx/sites-enabled
    ls /etc/apache2/sites-enabled
}
export -f onpssllist

onpsslmulti(){
    # https://serversforhackers.com/c/letsencrypt-with-haproxy
    ONP_SSL_ARRAY=$@
    ONP_SSL_EMAIL=ssl@onapp.ru
    on_ssl_input_array=($ONP_SSL_ARRAY)
    # echo ${my_array[@]}
    for i in "${on_ssl_input_array[@]}"
        do
            # echo -n "Установка пакета:  "
            # echo "....... $i"
            echo "~~~~~~~~~~~~ Получаем сертификат $i ~~~~~~~~~~~~"
            sudo certbot certonly --standalone -d $i --non-interactive --agree-tos --email $ONP_SSL_EMAIL
            echo -n "~~~~~~~~~~~~ Выполнение запроса для $i окончено, "
            # echo -n " копирую ключ для haproxy ---> "
            # cat /etc/letsencrypt/live/$i/fullchain.pem /etc/letsencrypt/live/$i/privkey.pem > /etc/ssl/private/$i.pem
            # echo " [OK] Ключ для haproxy скопирован, проверяю: ~~~~~~~~~~~~"
            # ls /etc/ssl/private/ | grep $i.pem
        done
    echo "~~~~~~~~~~~~ Все ключи на сервере, проверяю: ~~~~~~~~~~~~"
    ls /etc/ssl/private/
    # service haproxy restart
    unset ONP_SSL_EMAIL
    unset ONP_SSL_DOMAIN
}
export -f onpsslmulti

onp_suit(){
    w_bitrix
    w_memcached_test
    echo ""
    echo ""
    ls -la
    echo ""
}
export -f onp_suit

