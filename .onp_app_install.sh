#!/bin/bash
# onp_alias
#
#

export onp_app_install_title="Файл с app_install......[OK]"


####################################################################################
########                                   ОБЩИЕ                             ########
####################################################################################

onpapt () {
    sudo apt install -y $@
}
export -f onpapt


onpremove () {
    sudo apt remove --purge -y $@
}
export -f onpremove

onpremovehard () {
    sudo apt remove --purge -y $@
    onpufix
    onpufixhard
}
export -f onpremovehard


onp_server_lamp() {
    inst_webmin_lamp
}
export -f onp_server_lamp

onp_server_lamp_uninstall() {
    inst_webmin_lamp_uninstall
}
export -f onp_server_lamp_uninstall

onp_server_lemp_nginx() {
    inst_webmin_nginx
}
export -f onp_server_lemp_nginx





####################################################################################
########                                  SERVER                            ########
####################################################################################
inst_webmin_lamp () {
    echo "~~~~~~~~~~~~~~~~~~ Установка Webmin APACHE-FPM~~~~~~~~~~~~~~~~~~~~"
    cd $HOME/onp/ && wget -N http://software.virtualmin.com/gpl/scripts/install.sh && chmod +x $HOME/onp/install.sh && source $HOME/onp/install.sh
}
export -f inst_webmin_lamp


inst_webmin_lamp_uninstall () {
    echo "~~~~~~~~~~~~~~~~~~ Установка Webmin APACHE-FPM~~~~~~~~~~~~~~~~~~~~"
    cd $HOME/onp/ && wget -N http://software.virtualmin.com/gpl/scripts/install.sh && chmod +x $HOME/onp/install.sh && source $HOME/onp/install.sh --uninstall
}
export -f inst_webmin_lamp_uninstall



inst_webmin_nginx () {
    echo "~~~~~~~~~~~~~~~~~~ Установка Webmin NGINX-FPM ~~~~~~~~~~~~~~~~~~~~"
    cd $HOME/onp/ && wget -N http://software.virtualmin.com/gpl/scripts/install.sh && chmod +x $HOME/onp/install.sh && source $HOME/onp/install.sh --bundle LEMP
}
export -f inst_webmin_nginx






####################################################################################
########                                Программы                            ########
####################################################################################
inst-npm () {
    echo "~~~~~~~~~~~~~~~~~~ Установка NPM сложная ~~~~~~~~~~~~~~~~~~~~"
    (mkdir -p $HOME/.srv/npm && cd $HOME/.srv/npm  && sudo apt install -y npm nodejs && curl https://www.npmjs.com/install.sh | sudo sh)
}
export -f inst-npm


####################################################################################
########                                Тестирование                          ########
####################################################################################
onp_install_function () {
    sudo apt update -y && sudo apt install -y $ONP_INSTALL_VAR
    unset ONP_INSTALL_VAR
}
export -f onp_install_function

inst-cli () {
    ONP_INSTALL_VAR="htop glances"
    onp_install_function
}
export -f inst-cli

inst-test () {
    ONP_INSTALL_VAR="fio ioping"
    onp_install_function
}
export -f inst-test





inst-sysbench () {
    echo "~~~~~~~~~~~~~~~~~~ Установка sysbench ~~~~~~~~~~~~~~~~~~~~"
    curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash
    sudo apt -y install sysbench
}
export -f inst-sysbench


#Create mysql test DELETE ALL ROOT
inst-mysql () {
    echo "~~~~~~~~~~~~~~~~~~ Установка  mysql + тестового юзера ВСЕ СТИРАЕТ ROOT ~~~~~~~~~~~~~~~~~~~~"
    onpapt mariadb-server && (printf "\n y\n 123\n 123\n y\n y\n y\n y\n " | sudo mysql_secure_installation) && mysql -u root -e "CREATE DATABASE sbtest;" && mysql -u root -e "CREATE USER sbtest@localhost;" && mysql -u root -e "GRANT ALL PRIVILEGES ON sbtest.* TO sbtest@localhost"
}
 export -f inst-mysql


inst-onptest-mysql () {
    echo "~~~~~~~~~~~~~~~~~~ Установка Тестового окружения ~~~~~~~~~~~~~~~~~~~~"
    inst-sysbench
    inst-mysql
}
 export -f inst-onptest-mysql


install_onpd_locale_debian () {
    #Install ONPD LOCALE DEBIAN
    clear && echo "********** Установка onpd locale DEBIAN началась ******" && cd $HOME/onp && wget https://gitlab.com/cmilestone/ai_scripts/raw/master/.onp_install_only_console_debian.sh -N -q && /bin/bash .onp_install_only_console_debian.sh && echo "********** Установка onpd locale DEBIAN прошла успешно ******"
}
 export -f install_onpd_locale_debian



install_onpd_locale_ubuntu () {
    #Install ONPD LOCALE DEBIAN
    clear && echo "********** Установка onpd locale UBUNTU началась ******" && cd $HOME/onp && wget  https://gitlab.com/cmilestone/ai_scripts/raw/master/.onp_install_only_console_ubuntu.sh -N -q && /bin/bash .onp_install_only_console_ubuntu.sh && echo "********** Установка onpd locale UBUNTU прошла успешно ******"
}
 export -f install_onpd_locale_ubuntu