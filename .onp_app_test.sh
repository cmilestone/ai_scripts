#!/bin/bash
# onp_alias
#
#
export onp_app_title="Файл с app......[OK]"
# source $HOME/onp/.onp_config.sh

app_onp_install(){
    #Установка тестового пакета
    inst-sysbench
    inst-mysql
            ___onp_config_setup_line
}
export -f app_onp_install

app-unixbench () {
    echo "~~~~~~~~~~~~~~~~~~ Запуск unixbench для тестирования производительности ~~~~~~~~~~~~~~~~~~~~"
    sudo /bin/bash $HOME/onp/unixbench.sh
}
export -f app-unixbench



test_nench () {
    echo "~~~~~~~~~~~~~~~~~~ Запуск nench для тестирования производительности ~~~~~~~~~~~~~~~~~~~~"
    source $HOME/onp/.nench.sh
}
export -f test_nench

#oltp_read_only small
app-control() {
    echo "app_mysql_simple_read_test"
    sysbench oltp_read_only --threads=2 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=3 --table-size=500 prepare && sysbench oltp_read_only --threads=2 --events=0 --time=5 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=3 --table-size=500 --range_selects=off --db-ps-mode=disable --report-interval=1 run && sysbench oltp_read_only --threads=2 --events=0 --time=5 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=3 --table-size=500 --range_selects=off --db-ps-mode=disable --report-interval=1 cleanup
}
export -f app-control


#oltp_read_only small
app_mysql_read() {
    echo "app_mysql_read"
    sysbench oltp_read_only --threads=8 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=5 --table-size=500000 prepare && sysbench oltp_read_only --threads=8 --events=0 --time=60 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=5 --table-size=500000 --range_selects=off --db-ps-mode=disable --report-interval=1 run && sysbench oltp_read_only --threads=8 --events=0 --time=60 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=5 --table-size=500000 --range_selects=off --db-ps-mode=disable --report-interval=1 cleanup
}
export -f app_mysql_read


#oltp_read_write START
app_mysql_rw_start() {
    echo "app_mysql_rw_start"
    sysbench oltp_read_write --threads=16 --mysql-user=root --mysql-password=123 --tables=10 --table-size=1000000 prepare && sysbench oltp_read_write --threads=16 --events=0 --time=100 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=10 --table-size=1000000 --db-ps-mode=disable --report-interval=1 run && sysbench oltp_read_write --mysql-user=root --mysql-password=123 --tables=20 cleanup

}
export -f app_mysql_rw_start


#oltp_read_write HEAVY
app_mysql_rw_heavy() {
    echo "app_mysql_rw_heavy"
    sysbench oltp_read_write --threads=16 --mysql-user=root --mysql-password=123 --tables=15 --table-size=3000000 prepare && sysbench oltp_read_write --threads=16 --events=0 --time=100 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=15 --table-size=3000000 --delete_inserts=10 --index_updates=10 --non_index_updates=10 --db-ps-mode=disable --report-interval=1 run && sysbench oltp_read_write --mysql-user=root --mysql-password=123 --tables=15 cleanup
}
export -f app_mysql_rw_heavy


#oltp_read_write START
app_mysql_write() {
    echo "app_mysql_rw_start"
    sysbench insert_mysql --threads=16 --mysql-user=root --mysql-password=123 --tables=10 --table-size=1000000 prepare && sysbench insert_mysql --threads=16 --events=0 --time=100 --mysql-user=root --mysql-password=123 --mysql-port=3306 --tables=10 --table-size=1000000 --db-ps-mode=disable --report-interval=1 run && sysbench insert_mysql --mysql-user=root --mysql-password=123 --tables=20 cleanup

}
export -f app_mysql_write

#Файлы
app_fileio() {
    echo "app_fileio"
    sysbench fileio --file-total-size=9G prepare && sysbench fileio --file-total-size=9G --file-test-mode=rndrw  --max-time=100 --max-requests=1000000 run && sysbench fileio cleanup
}
export -f app_fileio


#Память
app_ramio() {
    echo "app_ramio"
    sysbench --test=memory run
}
export -f app_ramio


#CPU
app_cpuio() {
    echo "app_cpuio"
    sysbench --test=cpu --cpu-max-prime=20000 run
}
export -f app_cpuio


#ПОТОКИ
app_threads_1 () {
    echo "app_threads_1"
    sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run
}
export -f app_threads_1


#ПОТОКИ
app_threads_2 () {
    echo "app_threads_2"
    #sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run
    sysbench --num-threads=128 --test=threads --thread-yields=1000 --thread-locks=16 run
}
export -f app_threads_2

#ПОТОКИ
app_threads_3 () {
    echo "app_threads_3"
    sysbench --num-threads=256 --test=threads --thread-yields=2000 --thread-locks=32 run
}
export -f app_threads_3


onptest () {
        log_stamp
    log_time
    test_nench
        ___onp_config_setup_line
        log_time
    app_cpuio
        ___onp_config_setup_line
        log_time
        log_stamp
    app_ramio
        ___onp_config_setup_line
        log_time
        log_stamp
    app_fileio
        ___onp_config_setup_line
        log_time
        log_stamp
    app_mysql_read
    ___onp_config_setup_line
        log_time
        log_stamp
    app_mysql_rw_start
    ___onp_config_setup_line
    log_time
        log_stamp
    app_mysql_rw_heavy
        ___onp_config_setup_line
        log_time
    app_threads_1
        ___onp_config_setup_line
        log_time
    app_threads_2
        ___onp_config_setup_line
        log_time
        log_stamp
    app_threads_3
        ___onp_config_setup_line
        log_time
}
export -f onptest


onptest_log_a () {
    (echo -n "Начало теста   " && onpdate_log_hms && onptest && echo -n "Окончание теста   " && onpdate_log_hms ) | tee -a $HOME/onptest_log_a.log
}
export -f onptest_log_a


onptest_log_test () {
    (onpdate_log_hms && ___onp_config_log_dummy) | tee $HOME/onptest_log_test.log
    # cat $HOME/onptest_log_test.log
    clear
    cat $HOME/onptest_log_test.log
    sleep 4s
    rm $HOME/onptest_log_test.log
    app_mysql_simple_read_test
    ls -li
}
export -f onptest_log_test



