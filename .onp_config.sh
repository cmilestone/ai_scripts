#!/bin/bash
#
# onp_config_
# Версия приложения # ONAPP_VER_here Приложение ONP обновлено, версия ВЕРСИЯ
# Версия ONP ONAPP_VER ver onpver 286
export ONAPP_VER_NR_pre="- 05.09.2024 v.305"
export onp_config_title="Файл с глобальным конфигом......[OK]"
export onp_config_01="Введите общее название сервера ~~~~~~~~~~~ TIMEWEB 1CPU 3GB NVMe 80GB ~~~~~~~~~~~~"
export onp_config_02="Введите внешний адрес сервера для hostnamectl (вида subdomain.domain.ru)"
export onp_config_03="Введите вторую часть адреса сервера (вида subdomain)"
export onp_config_04="Введите хостера для обозначения promt консоли (вида HostHatch/Hetzner/AppleTec)"
export ONP_DATE_FUNC=$(date +%D%t%R:%S%t)


#
onpdate_log_hms() {
    echo $(date +%D%t%R:%S%t)
}
export -f onpdate_log_hms



onpupdate_files_func(){
    # Конфиг путей
    PATH_=$HOME/onp/
    # PATH_WWW_1=.onp_www_apps/
    PATH_WWW_2=$HOME/onp_www_apps/
    PATH_WWW_3=$HOME/onp_alni_server/
    COPY_="sudo cp $PATH_"
    CHMOD_="sudo chmod +x . $PATH_"
    sudo mkdir -p $PATH_WWW_2
    sudo mkdir -p $PATH_WWW_3
    # WWW_CP_=" $PATH_WWW_"

    # Скрипты инициализации
    FILE_=.onp_alias.sh && $COPY_$FILE_ $HOME/.onp_alias && unset FILE_
    FILE_=.onp_bashrc_colors.sh && $COPY_$FILE_ $HOME/.bashrc_colors && unset FILE_
    FILE_=.dir_colors.sh && $COPY_$FILE_ $HOME/.dir_colors && unset FILE_

    # Скрипты исполняемые
    FILE_=.onp_test_01.sh && $CHMOD_$FILE_ && unset FILE_

    # Файлы для разработки
    FILE_=.onp_www_memcached.php && $COPY_$FILE_ $PATH_WWW_2$FILE_ && unset FILE_
    FILE_=.bitrix_server_test.php && $COPY_$FILE_ $PATH_WWW_2$FILE_ && unset FILE_
    FILE_=.bitrixsetup.php && $COPY_$FILE_ $PATH_WWW_2$FILE_ && unset FILE_
}
export -f onpupdate_files_func



onpupdate_files_on_demand_func(){
    # Конфиг путей
    PATH_=$HOME/onp
    COPY_FROM_="/bin/cp -rf $PATH_"

    # Файлы по запросу исполняемые
    FILE_=.nano.zip && unzip -o -q $PATH_/$FILE_ -d $PATH_/
    $COPY_FROM_/nano/ ~/.nano/ && ls -la ~/.nano | grep vala
    rm -r -f $PATH_/nano && unset FILE_
}
export -f onpupdate_files_on_demand_func



___onp_config_setup_line () {
    echo " "
    echo " "
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo " "
    echo " "
}
export -f ___onp_config_setup_line

______onp_config_simple_line () {
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
# @func: Делает прочерк 11111
}
export -f ______onp_config_simple_line

log_time() {

    echo -n "~~~~~~~~~~~~~~ отметка времени: "
    echo -n $(date +%D%t%R:%S%t)
    echo -n " ~~~~~~~~~~~~~~~"
    echo -e ""
    # @func: Делает прочерк 222222
}

export -f log_time


log_stamp(){
    echo $(date +%Y-%m-%d-%H)
}
export -f log_stamp

______onp_config_log_line () {
    ONP_DATE_FUNC_CURRENT=$(onpdate_log_hms)
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~                     ЛОГ  $ONP_DATE_FUNC_CURRENT             ~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}
# @func: Делает прочерк 33333
export -f ______onp_config_log_line

___onp_config_log_dummy () {
    ONP_DATE_FUNC_CURRENT=$(onpdate_log_hms)
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~                     ЛОГ  $ONP_DATE_FUNC_CURRENT             ~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "С логом все в порядке, он работает. Ваша реклама могла бы быть здесь"
}
export -f ___onp_config_log_dummy

_________onp_config_setup_space () {
    echo " "
}
export -f _________onp_config_setup_space



onp_config_cli_color () {
    df -h
    echo "$onp_config_01"
    ______onp_config_simple_line
    read VARNAME_1
    ONP_SERVER_NAME_FUNC=$VARNAME_1
    echo "echo "~~~~~~~~~~~~~~  $VARNAME_1 ~~~~~~~~~~~~~~"" | sudo tee -a $HOME/.bashrc && source $HOME/.profile
    echo "alias aibash='source $HOME/.profile'" | tee -a $HOME/.bashrc > /dev/null
    echo "ONP_SERVER_NAME='$ONP_SERVER_NAME_FUNC'" | sudo tee -a $HOME/.bashrc > /dev/null && source $HOME/.profile
    cp $HOME/onp/.onp_alias.sh $HOME/.onp_alias
    echo "source $HOME/.onp_alias" | sudo tee -a $HOME/.bashrc > /dev/null

    echo "$onp_config_02"
    ______onp_config_simple_line
    read VARNAME_2
    ONP_SERVER_CLI_NAME_FUNC=$VARNAME_2
    ______onp_config_simple_line
    echo "$onp_config_03"
    read VARNAME_3
    ______onp_config_simple_line
    echo "$onp_config_04"
    read VARNAME_4
    hostnamectl set-hostname $VARNAME_2 && systemctl restart systemd-hostnamed && hostnamectl
    AI_IP=$(hostname -I | awk '{print $1}')
    AI_IN_1=$(echo -n "1 a \\" && echo -n "$AI_IP" && echo -n "  " && echo -n "$VARNAME_2" echo -n "  " && echo -n "$VARNAME_3")
    # AI_OUT_1=$(echo "$AI_IN_1")
    # sed -i "$AI_OUT_1" /etc/hosts
    sed -i "$AI_IN_1" /etc/hosts
    echo "ONP_SERVER_NAME_CLI='$ONP_SERVER_CLI_NAME_FUNC'" | sudo tee -a $HOME/.bashrc > /dev/null
    ONP_PROMT_ONP_FUNC=$VARNAME_4
    echo "ONP_PROMT_NAME='$ONP_PROMT_ONP_FUNC'" | sudo tee -a $HOME/.bashrc > /dev/null
    ONP_PROMT_NAME_ALIAS='$ONP_PROMT_NAME'
    onp_config_05_cli_color="PS1='\[\033[01;36m\]$ONP_PROMT_NAME_ALIAS \[\033[01;32m\]\u@\H\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$  '"
    echo "$onp_config_05_cli_color" | sudo tee -a $HOME/.bashrc > /dev/null && source $HOME/.profile
    cat /etc/hosts
}
export -f onp_config_cli_color




onp_config_disk_swap () {
    echo "~~~~~~~~~~~~~~~~~~ ДИСКИ системы ~~~~~~~~~~~~~~~~~~~~"
    df -h
    echo "~~~~~~~~~~~~~~~~~~ Проверка файла подкачки ~~~~~~~~~~~~~~~~~~~~"
    free -h
    sudo swapon --show
    echo "Введите желаемы размер файла подкачки в GB (до 6GB RAM рекомендуется такой же RAM/SWAP, свыше 6GB RAM указать половину от RAM/SWAP)"
    read VARNAME_4
    sudo fallocate -l "$VARNAME_4"G /swapfile
    sudo chmod 600 /swapfile
    sudo mkswap /swapfile
    sudo swapon /swapfile
    echo "/swapfile swap swap defaults 0 0" | sudo tee -a /etc/fstab
    echo "~~~~~~~~~~~~~~~~~~ Файл подкачки ~~~~~~~~~~~~~~~~~~~~"
    sudo free -h
    echo "~~~~~~~~~~~~~~~~~~ Старое значение баланса подкачки ~~~~~~~~~~~~~~~~~~~~"
    cat /proc/sys/vm/swappiness
    sudo sysctl vm.swappiness=20
    echo "~~~~~~~~~~~~~~~~~~ Новое значение баланса подкачки ~~~~~~~~~~~~~~~~~~~~"
    cat /proc/sys/vm/swappiness
}
export -f onp_config_disk_swap



onp_config_time () {
    echo "~~~~~~~~~~~~~~~~~~ Устанавливаем время  ~~~~~~~~~~~~~~~~~~~~"
    sudo apt install ntp ntpdate -y
    sudo dpkg-reconfigure ntp
    ntpq -p
    sudo ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
    sudo timedatectl set-timezone Europe/Moscow
    sudo hwclock
}
export -f onp_config_time



onp_config_end_update () {
    clear
    sudo apt dist-upgrade && sudo apt autoremove && lsb_release -a
    echo "~~~~~~~~~~~~~~~~~~ Все готово, войдите после презапуска системы снова  ~~~~~~~~~~~~~~~~~~~~"
    sudo reboot
}
export -f onp_config_end_update

onp_config_end_update_no_reboot () {
    clear
    sudo apt dist-upgrade && sudo apt autoremove && lsb_release -a
    echo "~~~~~~~~~~~~~~~~~~ Все готово, войдите после презапуска системы снова  ~~~~~~~~~~~~~~~~~~~~"
}
export -f onp_config_end_update_no_reboot

onp_config_simple_reboot () {
    echo "~~~~~~~~~~~~~~~~~~ Войдите после презапуска системы снова  ~~~~~~~~~~~~~~~~~~~~"
    sudo reboot
}
export -f onp_config_simple_reboot

# --- End Definitions Section ---
# check if we are being sourced by another script or shell
# [[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }




onp_multi_php_local_install(){
    echo "Мульти PHP"
}
export -f onp_multi_php_local_install



onp_config_end_server_help(){
cat << EOF
>>>>>>>>>>>>>> Действия для сервера
    ~~~~~~~~~~~~~~ #Добавить юзера alni: ~~~~~~~~~~~~~~
    >>>>>>> добавление нового юзера alni
    useradd -m -c "Alex" alni -s /bin/bash
    >>>>>>> пароль  alni
    passwd alni

    >>>>>>> Добавление рут доступа без sudo на alni или другого
    su root
    sudo usermod -a -G sudo alni
    sudo gpasswd -a alni sudo
    sudo -s
    adduser alni sudo
    >>>>>>> проверить что есть группа sudo
    groups
    >>>>>>> только под root
    sudo visudo
    >>>>>>> Добавляем
    alni        ALL=(ALL) NOPASSWD:ALL
EOF
}
export -f onp_config_end_server_help


onp_config_end_local_help(){
    echo "# Выключить firewall !!!! $sudo apt-mark hold ebtables"
    echo "#Сменить root пароль: $sudo -u root /bin/bash  , далее $ passwd"
}
export -f onp_config_end_local_help


onpdrepotest(){
    curl -v --silent https://gitlab.com/cmilestone/ai_scripts --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle="'
    # ONPDREPOTEST_VAR=$(curl -v --silent https://gitlab.com/cmilestone/ai_scripts --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle=")')
}
export -f onpdrepotest


onpdrepo_func(){
    ####### Проверяем условия есть ли обновление репо
    if [ -f $HOME/onp/check_repo_update_* ]; then
        ####### Если файл есть, то
        DATE_1=$(ls -1 $HOME/onp/check_repo_update_* | grep -oP '[\d]+')
        echo "Обновляю репозиторий"
        DATE_2_CHECK=$(curl -v --silent https://gitlab.com/cmilestone/ai_scripts/-/commits/master --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle=")' | head -1)
        DATE_2=$(echo $DATE_2_CHECK | xargs -I var_d date --date='var_d' +%s)
        DATE_3_CONTROL=$(echo $DATE_2_CHECK | xargs -I var_d date --date='var_d' +%d/%m/%Y__%T)
    else
        ####### Если файла нет, то
        DATE_2_CHECK=$(curl -v --silent https://gitlab.com/cmilestone/ai_scripts/-/commits/master --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle=")' | head -1)
        DATE_2=$(echo $DATE_2_CHECK | xargs -I var_d date --date='var_d' +%s)
        DATE_3_CONTROL=$(echo $DATE_2_CHECK | xargs -I var_d date --date='var_d' +%d/%m/%Y—%T)
        touch $HOME/onp/check_repo_update_$DATE_2
        DATE_1=$(ls -1 $HOME/onp/check_repo_update_* | grep -oP '[\d]+')
        #echo "Проверка onpdrepo_func else"
    fi;

    ####### Выполняем условия
    if [ $DATE_1 -ge $DATE_2 ]; then
        ####### Изменений нет
        # export onpdstatus="DATE_1 такое же, repo не изменился"
        echo "Изменений репозитория нет"
        echo "Метка обновления $DATE_3_CONTROL"
    else
        ####### Изменения есть
        echo "Есть изменения в репозитории, начинаю обновление"
        onpdf
        echo "Обновление репозитория завершено"
        rm -f $HOME/onp/check_repo_update_*
        touch $HOME/onp/check_repo_update_$DATE_2
        # echo "Дата обновления $DATE_3_CONTROL   |   дата файла *$DATE_2"
        echo "Метка обновления $DATE_3_CONTROL"
    fi;
}
export -f onpdrepo_func

onplocalcheckdir(){
    ####### Выставляем кастомный bash
    # DIR_01_IN=$(pwd | sed 's,[^/]*$,,')
    # DIR_02_WORD=$(pwd | sed 's#.*/##')
    # DIR_03_FULL=$(pwd)
    # CODE_PWD=/mnt/d/code
    CURR_PWD=$(pwd | grep /mnt/d/code)
    if [ -z "$CURR_PWD" ]; then
        ####### Если это папка не с кодами, то
        unset CURR_PWD
    else
        ####### Если это папка с кодами, то
        source ~/.profile
        source /mnt/c/alniconf/bash/.bash_local_profile
        # source /mnt/d/code/.aic_alias.sh
        unset CURR_PWD
    fi;
}
export -f onplocalcheckdir


onpdrepotest(){
    echo "Последняя работающая формула запроса:"
    curl -v --silent https://gitlab.com/cmilestone/ai_scripts/commits/master/ --stderr - | grep  -m 1 -oP '(?<=datetime=")(.*?)(?=" data-toggle=")'
    #curl -v --silent https://gitlab.com/cmilestone/ai_scripts --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle=")'
    #curl -v --silent https://gitlab.com/cmilestone/ai_scripts/commits/master/ --stderr - | grep  commit-author-link
    #curl -v --silent https://gitlab.com/cmilestone/ai_scripts/commits/master/ --stderr - | grep  -oP '(?<=datetime=")(.*?)(?=" data-toggle=")'
}
export -f onpdrepotest


onpcron(){
    sudo crontab -e
    sudo crontab -l
}
export -f onpcron

onptail(){
    sudo tail -n 80 -f /var/log/fail2ban.log
}
export -f onptail


onptailban(){
    sudo tail -n 160 -f /var/log/fail2ban.log | grep already
}
export -f onptailban

onptailwarn(){
    sudo tail -n 160 -f /var/log/fail2ban.log | grep WARNING
}
export -f onptailwarn


onptailnotice(){
    sudo tail -n 160 -f /var/log/fail2ban.log | grep NOTICE
}
export -f onptailnotice


onpmail(){
    ONP_MAIN_01=$(hostname)
    ONP_MAIN_02=$(date +%H:%M_%d-%m-%Y)
    echo "$1 $ONP_MAIN_01 test Текст тела письма" | mail -s "$ONP_MAIN_02 $1 Письмо  $ONP_MAIN_01" root
}
export -f onpmail



onpcolor(){
    for x in {0..8}; do
        for i in {30..37}; do
            for a in {40..47}; do
                echo -ne "\e[$x;$i;$a""m\\\e[$x;$i;$a""m\e[0;37;40m "
            done
            echo
        done
    done
    echo ""
}
export -f onpcolor



ONP_DATE_1=$(ls -1 $HOME/onp/check_repo_update_* | grep -oP '[\d]+')
# echo "Тест даты--------------"
ONP_DATE_2=$(date -d @$ONP_DATE_1 +"%d-%m-%Y   %T")
ONP_DATE_2_STATUS=$(date -d @$ONP_DATE_1 +"%d-%m-%Y    %H:%M")

DATE_5_VER_IN=$(ls -1 $HOME/onp/check_repo_update_* | grep -oP '[\d]+')
DATE_5_VER=$(date -d @$DATE_5_VER_IN +'%m.%d')




# export ONAPP_VER_NR="$ONAPP_VER_NR_pre$DATE_5_VER"
export ONAPP_VER_NR="$ONAPP_VER_NR_pre"
export ONAPP_VER="$ONAPP_VER_NR  |  $ONP_DATE_2_STATUS"


# TODO: onplistfunc Сделать скрипт для листинга всех функций
onplistfunc(){
    echo "(\#+ \@func: .* )|(-f+\s+(.*\w))"
}
export -f onplistfunc
