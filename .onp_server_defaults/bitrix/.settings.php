<?php

return array (
  'utf_mode' =>
  array (
    'value' => true,
    'readonly' => true,
  ),
  'cache' => array(
        'value' => array(
            'type' => array(
                'class_name' => '\\Bitrix\\Main\\Data\\CacheEngineMemcache',
                'extension' => 'memcache'
            ),
            'memcache' => array(
                'host' => '127.0.0.1',
                'port' => '11211',
            )
        ),
        'sid' => $_SERVER["DOCUMENT_ROOT"]."#01"
    ),

  'cache_flags' =>
  array (
    'value' =>
    array (
      'config_options' => 3600.0,
      'site_domain' => 3600.0,
    ),
    'readonly' => false,
  ),
  'cookies' =>
  array (
    'value' =>
    array (
      'secure' => false,
      'http_only' => true,
    ),
    'readonly' => false,
  ),
  'exception_handling' =>
  array (
    'value' =>
    array (
      'debug' => false,
      'handled_errors_types' => 4437,
      'exception_errors_types' => 4437,
      'ignore_silence' => false,
      'assertion_throws_exception' => true,
      'assertion_error_type' => 256,
      'log' => NULL,
    ),
    'readonly' => false,
  ),
  'connections' =>
  array (
    'value' =>
    array (
      'default' =>
      array (
        'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
        'host' => 'localhost',
        'database' => 'server5_2_onapp_ru',
        'login' => 'server5_2_onapp_ru',
        'password' => '12345',
        'options' => 2.0,
      ),
    ),
    'readonly' => true,
  ),
  'crypto' =>
  array (
    'value' =>
    array (
      'crypto_key' => '58198d70adca5edd5e90433d410330cd',
    ),
    'readonly' => true,
  ),
);
