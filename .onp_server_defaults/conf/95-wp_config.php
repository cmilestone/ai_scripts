<?php
// # *******************************************************************************************#
// #         Этот файл должен быть в папке nano /etc/haproxy/haproxy.cfg                        #
// # *****/*************************************************************************************#
// в случае с VARNISH
// https://guides.wp-bullet.com/fix-varnish-mixed-content-errors-cloudflare-ssl-wordpress/
define('FORCE_SSL_ADMIN', true);

if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
        $_SERVER['HTTPS']='on';